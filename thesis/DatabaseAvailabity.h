//
//  DatabaseAvailabity.h
//  thesis
//
//  Created by horkavlna on 21/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#ifndef thesis_DatabaseAvailabity_h
#define thesis_DatabaseAvailabity_h

#define SigninDatabaseAvailabityNotification @"SigninDatabaseAvailabityNotification"
#define ConfigDatabaseAvailabityNotification @"ConfigDatabaseAvailabityNotification"
#define MenuDatabaseAvailabityNotification @"MenuDatabaseAvailabityNotification"
#define EventDatabaseAvailabityNotification @"EventDatabaseAvailabityNotification"
#define AlertNewDatabaseAvailabityNotification @"AlertNewDatabaseAvailabityNotification"
#define AlertExistDatabaseAvailabityNotification @"AlertExistDatabaseAvailabityNotification"
#define DetailNewAlertNotification @"DetailNewAlertNotification"
#define DetailExistAlertNotification @"DetailExistAlertNotification"
#define DetailEventNotification @"DetailEventNotification"
#define ContainerFilterNotification @"ContainerFilterNotification"
#define FilterOneNotification @"FilterOneNotification"
#define FilterSearchNotification @"FilterSearchNotification"
#define FilterSelected @"FilterSelected"

#define MenuRemotePushNotification @"MenuRemotePushNotification"
#define AlertRemotePushNotification @"AlertRemotePushNotification"

#define OrientationNotification @"OrientationNotification"

#define WrongNotification @"wrongNotification"

#define DatabaseAvailabityContext @"Context"
#define AlertObject @"AlertObject"
#define EventObject @"EventObject"
#define AlertId @"AlertId"
#define Logout @"Logout"
#define FilterObject @"FilterObject"

#endif
