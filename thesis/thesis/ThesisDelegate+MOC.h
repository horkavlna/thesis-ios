//
//  ThesisDelegate+MOC.h
//  thesis
//
//  Created by horkavlna on 21/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "ThesisDelegate.h"

@interface ThesisDelegate (MOC)

- (NSManagedObjectContext *)createMainQueueManagedObjectContext;

@end
