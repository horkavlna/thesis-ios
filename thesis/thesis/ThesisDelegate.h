//
//  AppDelegate.h
//  thesis
//
//  Created by horkavlna on 13/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThesisDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
