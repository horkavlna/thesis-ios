//
//  Alert.m
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Alert.h"
#import "User.h"


@implementation Alert

@dynamic color;
@dynamic confirmed;
@dynamic created;
@dynamic device;
@dynamic idAlert;
@dynamic img;
@dynamic instructions;
@dynamic place;
@dynamic title;
@dynamic type;
@dynamic whoOwnAlert;

@end
