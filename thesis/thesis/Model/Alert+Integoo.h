//
//  Alert+Integoo.h
//  thesis
//
//  Created by horkavlna on 25/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Alert.h"

#define ALERT_IDINTEGOO @"id"
#define ALERT_NAME @"title"
#define ALERT_DEVICE @"device"
#define ALERT_PLACE @"place"
#define ALERT_CREATED @"created"
#define ALERT_COLOR @"color"
#define ALERT_CONFIRMED @"confirmed"
#define ALERT_IMG @"img"
#define ALERT_INSTRUCTIONS @"instructions"

#define ALERT_TYPE @"type"

@interface Alert (Integoo)

+ (void) loadAlertsFromIntegooArray: (NSArray *)newAlertArray intoManagedObjectContext:(NSManagedObjectContext *)context
                           withType:(NSNumber *)type withUser:(User *)user;
+ (void) deleteAlertPressConfirmButton: (Alert *)alert intoManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSMutableArray*) fetchAllAlerts: (NSManagedObjectContext *)context withType:(NSNumber *)type withUser: (User *) user;
+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldAlerts compareNewArray: (NSMutableArray *)newArray
              inManagedObjectContext: (NSManagedObjectContext *)context withType:(NSNumber *)type withUser: (User *) user;
+ (void) saveImage: (Alert *)alertOld addImage:(NSArray *)arrayImages intoManagedObjectContext:(NSManagedObjectContext *)context;
+ (Alert *) findAlertFromRemotePushNotification: (NSManagedObjectContext *)context
                                    withAlertId: (NSNumber *)idAlert withType:(NSNumber *)type withUser: (User *) user;
@end
