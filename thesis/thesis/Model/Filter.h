//
//  Filter.h
//  integoo
//
//  Created by horkavlna on 17/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event, User;

@interface Filter : NSManagedObject

@property (nonatomic, retain) NSNumber * idFilter;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * isChecked;
@property (nonatomic, retain) NSSet *events;
@property (nonatomic, retain) User *whoOwnFilter;
@end

@interface Filter (CoreDataGeneratedAccessors)

- (void)addEventsObject:(Event *)value;
- (void)removeEventsObject:(Event *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

@end
