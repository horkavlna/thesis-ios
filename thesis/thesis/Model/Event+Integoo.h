//
//  Event+Integoo.h
//  thesis
//
//  Created by horkavlna on 21/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Event.h"
#import "User.h"

#define EVENT_IDINTEGOO @"id"
#define EVENT_NAME @"message"
#define EVENT_DEVICE @"device"
#define EVENT_PLACE @"place"
#define EVENT_CREATED @"created"
#define EVENT_IMG_ICON @"imgicon"

@interface Event (Integoo)

+ (void) saveEvents: (NSDictionary *)eventDictionary inManagedObjectContext: (NSManagedObjectContext *)context
         withFilter: (Filter *)filter withUser: (User *)user;
+ (void) saveImage: (Event*)eventOld addImage:(NSArray *)arrayImage intoManagedObjectContext:(NSManagedObjectContext *)context withFilter: (Filter *)filter;
+ (NSMutableArray*) fetchAllEvents: (NSManagedObjectContext *)context withFilter: (Filter *)filter;
+ (void) loadEventsFromIntegooArray: (NSArray *)newEventArray intoManagedObjectContext:(NSManagedObjectContext *)context withFilter: (Filter *)filter withUser: (User *)user;
+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldAlerts compareNewArray: (NSMutableArray *)newArray inManagedObjectContext: (NSManagedObjectContext *)context withFilter: (Filter *)filter;

@end
