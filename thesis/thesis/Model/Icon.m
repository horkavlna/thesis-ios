//
//  Icon.m
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Icon.h"
#import "Event.h"
#import "User.h"


@implementation Icon

@dynamic idImage;
@dynamic img;
@dynamic whoOwnIcon;
@dynamic events;

@end
