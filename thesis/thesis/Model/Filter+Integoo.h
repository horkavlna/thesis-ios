//
//  Filter+Integoo.h
//  thesis
//
//  Created by horkavlna on 18/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Filter.h"

#define FILTER_IDINTEGOO @"id"
#define FILTER_NAME @"name"

@interface Filter (Integoo)

+ (void) saveFilter: (NSDictionary *)filterDictionary inManagedObjectContext: (NSManagedObjectContext *)context withUser:(User *)user;
+ (void) loadFiltersFromIntegooArray: (NSArray *)filterArray intoManagedObjectContext:(NSManagedObjectContext *)context withUser:(User *)user;
+ (NSMutableArray*) fetchAllFilters: (NSManagedObjectContext *)context withUser: (User *) user;
+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldFilters compareNewArray: (NSArray *)newFilters inManagedObjectContext: (NSManagedObjectContext *)context withUser:(User *)user;
+ (Filter*) fetchFilter:(NSNumber *)idFilter inManagedObjectContext: (NSManagedObjectContext *)context withUser:(User *)user;
+ (Filter*) fetchSelectedFilterManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user;

@end
