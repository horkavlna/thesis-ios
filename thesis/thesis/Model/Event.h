//
//  Event.h
//  integoo
//
//  Created by horkavlna on 15/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Filter, Icon;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * device;
@property (nonatomic, retain) NSNumber * idEvent;
@property (nonatomic, retain) NSData * img;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * user;
@property (nonatomic, retain) Filter *whichFilter;
@property (nonatomic, retain) Icon *whichIcon;

@end
