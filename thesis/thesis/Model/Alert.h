//
//  Alert.h
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Alert : NSManagedObject

@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSNumber * confirmed;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * device;
@property (nonatomic, retain) NSNumber * idAlert;
@property (nonatomic, retain) NSData * img;
@property (nonatomic, retain) NSString * instructions;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) User *whoOwnAlert;

@end
