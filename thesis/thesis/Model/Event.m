//
//  Event.m
//  integoo
//
//  Created by horkavlna on 15/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Event.h"
#import "Filter.h"
#import "Icon.h"


@implementation Event

@dynamic created;
@dynamic device;
@dynamic idEvent;
@dynamic img;
@dynamic message;
@dynamic place;
@dynamic user;
@dynamic whichFilter;
@dynamic whichIcon;

@end
