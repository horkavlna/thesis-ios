//
//  Support.m
//  thesis
//
//  Created by horkavlna on 14/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Support.h"

@implementation Support

NSString *const DEVICETOKEN = @"token";
NSString *const SERVERADDRESS = @"serveraddress";
NSString *const REMOTEPUSHNOTIFICATION = @"remote";
NSString *const TIMELOGOUT = @"timelogout";
NSString *const TIMELOGOUTSWITCHER = @"timelogoutswitcher";
NSString *const TIMEBACKGROUND = @"timebackground";
NSString *const DEFAULTUSER = @"defaultuser";
NSString *const USER = @"user";
NSString *const USERNAME = @"username";
NSString *const PASSWORD = @"password";
NSString *const LIMITFATCH = @"limitfatch";
NSString *const FILTERNAME = @"filtername";
NSString *const FILTERID = @"filterid";


#pragma default user
+ (BOOL)saveDefaultUser: (NSString *) defaultUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:defaultUser forKey:DEFAULTUSER];
    return [defaults synchronize];
}

+ (NSString *)getDefaultUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:DEFAULTUSER];
}

+ (BOOL)saveUser: (NSString *)userName withPassword: (NSString *)password
{
    NSDictionary *login = @{USERNAME:userName, PASSWORD:password};
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:login forKey:USER];
    return [defaults synchronize];
}

+ (NSDictionary *)getUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:USER];
}

+ (NSString *)getUsername
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *login = [defaults objectForKey:USER];
    return [login objectForKey:USERNAME];
}

+ (NSString *)getPassword
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *login = [defaults objectForKey:USER];
    return [login objectForKey:PASSWORD];
}

+ (NSString *)getUsernameAndPasswordForBase64Encoded: (NSString *) userName withPassword: (NSString *) password
{
    if ((userName == nil) && (password == nil)) {
        NSDictionary *userAndPassword = [Support getUser];
        if (userAndPassword != nil) {
            userName = [userAndPassword objectForKey:USERNAME];
            password = [userAndPassword objectForKey:PASSWORD];
        } else {
            return nil;
        }
    }
    NSMutableString *usernameAndPasswordForBase64Encoded = [[NSMutableString alloc] init];
    [usernameAndPasswordForBase64Encoded appendString:userName];
    [usernameAndPasswordForBase64Encoded appendString:@":"];
    [usernameAndPasswordForBase64Encoded appendString:password];
    NSString *usernameAndPassrowdBase64 = [[usernameAndPasswordForBase64Encoded dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    [usernameAndPasswordForBase64Encoded setString:@"Basic "];
    [usernameAndPasswordForBase64Encoded appendString:usernameAndPassrowdBase64];
    return usernameAndPasswordForBase64Encoded;
}

+ (void)removeUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:USER];
}

+ (void) showAlertTitle: (NSString *)title withMessage: (NSString *)message withDelegate: (id)delegate cancelButton: (NSString *) cancelButtonTitle otherButton:(NSString *) otherButtonTitle withIdentifier: (NSInteger) tagId
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitle, nil];
    alert.tag = tagId;
    [alert show];
}

+ (UIColor *) standardColorForSettingsCellTableView
{
    return [UIColor colorWithRed:0.204 green:0.459 blue:1.000 alpha:1.0];
}

#pragma NetworkConnection
+ (void) wrongAuthentication: (NSString *) invalidData withLable: (UILabel *)invalidInputData
{
    __block CGRect frameInvalidInputData = invalidInputData.frame;
    frameInvalidInputData.origin.y = frameInvalidInputData.origin.y - 25;
    [invalidInputData setFrame:frameInvalidInputData];
    [invalidInputData setHidden:NO];
    [invalidInputData setText:invalidData];
    [invalidInputData setBackgroundColor:[UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f]];
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveLinear
                     animations:^{
                         frameInvalidInputData.origin.y = frameInvalidInputData.origin.y + 25;
                         invalidInputData.frame = frameInvalidInputData;
                     } completion:^(BOOL completed){
                         [UIView animateWithDuration:1.0f delay:2.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveLinear
                                          animations:^{
                                              frameInvalidInputData.origin.y = frameInvalidInputData.origin.y - 25;
                                              invalidInputData.frame = frameInvalidInputData;
                                          } completion:^(BOOL compleded){
                                              [invalidInputData setHidden:YES];
                                              frameInvalidInputData.origin.y = frameInvalidInputData.origin.y + 25;
                                              invalidInputData.frame = frameInvalidInputData;
                                              NSLog(@"completition");
                                              [[NSNotificationCenter defaultCenter] postNotificationName:WrongNotification object:nil];
                                          }];
                     }];
}

+ (NSData*)blobToNSData: (NSArray*) blob
{
    NSMutableData *data = [NSMutableData dataWithCapacity:blob.count];
    for (NSNumber *number in blob) {
        uint8_t byte = (uint8_t)[number intValue];
        [data appendBytes:&byte length:1];
    }
    return data;
}

#pragma DeviceToken
+ (BOOL)saveDeviceToken: (NSString *) token
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:token forKey:DEVICETOKEN];
    return [defaults synchronize];
}

+ (NSString *)getDeviceToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
#warning ONLY SIMULATOR REMOVE FOR DEVICE
    return @"2";
//    return [defaults objectForKey:DEVICETOKEN];
}

#pragma TimerLogout
//TIMELOGOUT is in seconds
+ (BOOL)saveTimeAutomaticLogout: (NSTimeInterval) time
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithDouble:time] forKey:TIMELOGOUT];
    return [defaults synchronize];
}

+ (NSTimeInterval)getTimeAutomaticLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *timeLogout = [defaults objectForKey:TIMELOGOUT];
    if (timeLogout == nil) {
        timeLogout = [NSNumber numberWithInt:1800];
    }
    return [timeLogout doubleValue];
}

+ (BOOL) saveTimeLogoutSwitcher:(BOOL) valueSwitcher
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:valueSwitcher] forKey:TIMELOGOUTSWITCHER];
    return [defaults synchronize];
}


+ (BOOL)getTimeLogoutSwitcher
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *timeLogoutSwitcher = [defaults objectForKey:TIMELOGOUTSWITCHER];
    if ([timeLogoutSwitcher intValue] == 0) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL) saveLastTimeUserInApplication
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    [defaults setObject:date forKey:TIMEBACKGROUND];
    return [defaults synchronize];
}

+ (NSTimeInterval) getLastTimeUserInApplication
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *timeBackground = [defaults objectForKey:TIMEBACKGROUND];
    return [timeBackground timeIntervalSince1970];
}


#pragma RemotePushNotification
//defaultValue = 0 - registr, registr = 1, unregistre = 2
+ (NSNumber *) getRegisterForRemoteNotification
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"registr RemotePushNotification: %@",[defaults objectForKey:REMOTEPUSHNOTIFICATION]);
    return [defaults objectForKey:REMOTEPUSHNOTIFICATION];
}

+ (BOOL) registerForRemoteNotification
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:1] forKey:REMOTEPUSHNOTIFICATION];
    return [defaults synchronize];
}

+ (BOOL) unregisterForRemoteNotification
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:2] forKey:REMOTEPUSHNOTIFICATION];
    return [defaults synchronize];
}

#pragma ServerAddress
+ (BOOL)saveServerAddress: (NSString *) serverAddress
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:serverAddress forKey:SERVERADDRESS];
    return [defaults synchronize];
}

+ (NSString *)getServerAddress
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:SERVERADDRESS];
}

+ (NSURL *)getWholeAddress: (NSString *)appendPostfixAddress
{
    NSMutableString *wholeAddress = [[NSMutableString alloc] initWithString:[self getServerAddress]];
    [wholeAddress appendString:appendPostfixAddress];
    return [NSURL URLWithString:wholeAddress];
}

#pragma LimitOneFetch
+ (BOOL)saveLimitFetch: (NSNumber *) limitFetch
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:limitFetch forKey:LIMITFATCH];
    return [defaults synchronize];
}

+ (NSNumber *)getLimitFetch
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:LIMITFATCH];
}

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert withAlpha:(float) alpha
{
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $
    
    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:alpha];
}

+ (NSArray *)getFetchLimitItem
{
    return @[@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55",@"60",@"65",@"70",@"75",@"80",@"85",@"90",@"95",@"100"];
}

#pragma Timer Alarm, Events
+ (NSString *) durationTime:(NSTimeInterval) seconds
{
    NSMutableString *outputTime = [[NSMutableString alloc] init];
    
    int days = seconds / (60 * 60 * 24);
    seconds -= days * (60 * 60 * 24);
    int hours = seconds / (60 * 60);
    seconds -= hours * (60 * 60);
    int minutes = seconds / 60;
    seconds -=minutes*60;
    
    if (days > 0) {
        [outputTime appendFormat:@"%d d ",days];
    }
    if (hours > 0) {
        if (hours < 10) {
            [outputTime appendFormat:@"0"];
        }
        [outputTime appendFormat:@"%d:",hours];
    } else {
        [outputTime appendFormat:@"00:"];
    }
    if (minutes > 0) {
        if (minutes < 10) {
            [outputTime appendFormat:@"0"];
        }
        [outputTime appendFormat:@"%d:",minutes];
    } else {
        [outputTime appendFormat:@"00:"];
    }
    if (seconds > 0) {
        if (seconds < 10) {
            [outputTime appendFormat:@"0"];
        }
        [outputTime appendFormat:@"%d",(int)seconds];
    } else {
        [outputTime appendFormat:@"00"];
    }
    return outputTime;
}

+ (NSDate *) parseJsonDateToNSDate: (NSString *) timeJson
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    NSDate *date = [formatter dateFromString:timeJson];
    return date;
}

+ (CGRect)getScreenFrameForOrientation:(UIInterfaceOrientation)orientation{
    
    UIScreen *screen = [UIScreen mainScreen];
    CGRect fullScreenRect = screen.bounds;
    BOOL statusBarHidden = [UIApplication sharedApplication].statusBarHidden;
    
    //implicitly in Portrait orientation.
    if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft){
        CGRect temp = CGRectZero;
        temp.size.width = fullScreenRect.size.height;
        temp.size.height = fullScreenRect.size.width;
        fullScreenRect = temp;
    }
    
    if(!statusBarHidden){
        CGFloat statusBarHeight = 20;//Needs a better solution, FYI statusBarFrame reports wrong in some cases..
        fullScreenRect.size.height -= statusBarHeight;
    }
    
    return fullScreenRect;
}

+ (CGSize)sizeOfText:(NSString *)textToMesure widthOfTextView:(CGFloat)width withFont:(UIFont*)font
{
    CGSize ts = [textToMesure sizeWithFont:font constrainedToSize:CGSizeMake(width-20.0, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    return ts;
}

@end
