//
//  Filter.m
//  integoo
//
//  Created by horkavlna on 17/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Filter.h"
#import "Event.h"
#import "User.h"


@implementation Filter

@dynamic idFilter;
@dynamic name;
@dynamic isChecked;
@dynamic events;
@dynamic whoOwnFilter;

@end
