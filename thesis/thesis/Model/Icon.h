//
//  Icon.h
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event, User;

@interface Icon : NSManagedObject

@property (nonatomic, retain) NSNumber * idImage;
@property (nonatomic, retain) NSData * img;
@property (nonatomic, retain) User *whoOwnIcon;
@property (nonatomic, retain) NSSet *events;
@end

@interface Icon (CoreDataGeneratedAccessors)

- (void)addEventsObject:(Event *)value;
- (void)removeEventsObject:(Event *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

@end
