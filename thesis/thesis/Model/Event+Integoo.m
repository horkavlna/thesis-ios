//
//  Event+Integoo.m
//  thesis
//
//  Created by horkavlna on 21/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Event+Integoo.h"
#import "Icon+Integoo.h"
#import "Support.h"
#import "Filter+Integoo.h"

@implementation Event (Integoo)

+ (void) saveEvents: (NSDictionary *)eventDictionary inManagedObjectContext: (NSManagedObjectContext *)context withFilter: (Filter *)filter withUser: (User *)user
{
    Event *event = nil;
    NSNumber *idIntegoo = eventDictionary[EVENT_IDINTEGOO];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whichFilter == %@) AND (idEvent == %@))", filter, idIntegoo];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || (matches.count > 1)) {
    } else if ([matches count]) {
    } else {
        event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:context];

        event.idEvent = eventDictionary[EVENT_IDINTEGOO];
        event.message = eventDictionary[EVENT_NAME];
        event.device = eventDictionary[EVENT_DEVICE];
        event.place = eventDictionary[EVENT_PLACE];
        event.created = [Support parseJsonDateToNSDate:[NSString stringWithFormat:@"%@",eventDictionary[EVENT_CREATED]]];

        event.whichFilter = filter;
        event.whichIcon = [Icon findIcon:context withIconId:eventDictionary[EVENT_IMG_ICON] withUser:user];
    }
}

+ (void) saveImage: (Event*)eventOld addImage:(NSArray *)arrayImage intoManagedObjectContext:(NSManagedObjectContext *)context withFilter: (Filter *)filter
{
    Event *event = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whichFilter == %@) AND (idEvent == %@))", filter, eventOld.idEvent];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (error != nil) {
    }
    else if (matches.count == 1) {
        event = [matches lastObject];
        event.img = [Support blobToNSData:[[NSMutableArray alloc] initWithArray:[arrayImage valueForKey:IMAGE_IMG]]];
    }
}

+ (NSMutableArray*) fetchAllEvents: (NSManagedObjectContext *)context withFilter: (Filter *)filter
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    request.predicate = [NSPredicate predicateWithFormat:@"whichFilter == %@", filter];
    NSError *error;
    NSMutableArray *matches = [NSMutableArray arrayWithArray: [context executeFetchRequest:request error:&error]];
    if (!matches || error ) {
        NSLog(@"%@ error deleteAllEvents %@ matches %d", [[Event class] description], error, [matches count]);
        return nil;
    } else {
        NSLog(@"%@ etchAllEvents count %d", [[Event class] description], [matches count]);
        return matches;
    }
}

+ (void) loadEventsFromIntegooArray: (NSArray *)newEventArray intoManagedObjectContext:(NSManagedObjectContext *)context withFilter: (Filter *)filter withUser: (User *)user
{
    for (NSDictionary *eventDictionary in newEventArray) {
        [self saveEvents:eventDictionary inManagedObjectContext:context withFilter:filter withUser:user];
    }
}

+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldAlerts compareNewArray: (NSMutableArray *)newArray inManagedObjectContext: (NSManagedObjectContext *)context withFilter: (Filter *)filter
{
    for (NSDictionary *filterDictionary in newArray) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((whichFilter == %@) AND (idEvent == %@))", filter, [NSNumber numberWithInt:[filterDictionary[EVENT_IDINTEGOO] integerValue]]];
        NSArray *results = [oldAlerts filteredArrayUsingPredicate:predicate];
        if ([results count] != 0) {
            Event *event = [results lastObject];
            [oldAlerts removeObject:event];
        }
    }
    for (Event *event in oldAlerts) {
        [context deleteObject:event];
    }
}

@end
