//
//  Icon+Integoo.m
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Icon+Integoo.h"

@implementation Icon (Integoo)

+ (void) saveIcon: (NSDictionary *)iconDictionary inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user
{
    Icon *icon = nil;
    NSNumber *idIntegoo = iconDictionary[IMAGE_ID_IMG];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Icon"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnIcon == %@) AND (idImage == %@))", user, idIntegoo];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || (matches.count > 1)) {
    } else if ([matches count]) {
    } else {
        icon = [NSEntityDescription insertNewObjectForEntityForName:@"Icon" inManagedObjectContext:context];
        icon.img = [Support blobToNSData:[[NSMutableArray alloc] initWithArray:iconDictionary[IMAGE_IMG]]];
        icon.idImage = idIntegoo;
        icon.whoOwnIcon = user;
        NSLog(@"%@ save icon with id %@", [[Icon class] description], idIntegoo);
    }
}

+ (void) loadIconsFromIntegooArray: (NSArray *)iconArray intoManagedObjectContext:(NSManagedObjectContext *)context withUser: (User *) user
{
    for (NSDictionary *iconDictionary in iconArray) {
        [self saveIcon:iconDictionary inManagedObjectContext:context withUser:user];
    }
}

+ (Icon *) findIcon: (NSManagedObjectContext *)context withIconId: (NSNumber *)idIcon withUser: (User *) user
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Icon"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnIcon == %@) AND (idImage == %@))", user, idIcon];
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    if (!matches || error || matches.count == 0 ) {
        NSLog(@"%@ icon doesn't exist", [[Icon class] description]);
        return nil;
    } else {
        NSLog(@"%@ findIcon finded object %@", [[Icon class] description], [matches firstObject]);
        return [matches firstObject];
    }
}

+ (BOOL) existIcon: (NSManagedObjectContext *)context withUser: (User *) user
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Icon"];
    request.predicate = [NSPredicate predicateWithFormat:@"whoOwnIcon == %@", user];
    NSError *error;
    NSMutableArray *matches = [NSMutableArray arrayWithArray: [context executeFetchRequest:request error:&error]];
    if (!matches || error ) {
        NSLog(@"%@ error deleteAllIcons %@ matches %d", [[Icon class] description], error, [matches count]);
        return NO;
    } else {
        NSLog(@"%@ existIcon %d",[[Icon class] description],[matches count]);
        return YES;
    }
}

+ (NSMutableArray*) fetchAllIcon: (NSManagedObjectContext *)context withUser: (User *) user
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Icon"];
    request.predicate = [NSPredicate predicateWithFormat:@"whoOwnIcon == %@", user];
    NSError *error;
    NSMutableArray *matches = [NSMutableArray arrayWithArray: [context executeFetchRequest:request error:&error]];
    if (!matches || error ) {
        NSLog(@"%@ error deleteAllIcon %@ matches %d", [[Icon class] description], error, [matches count]);
        return nil;
    } else {
        NSLog(@"%@ fetchAllIcon count %d", [[Icon class] description], [matches count]);
        return matches;
    }
}

+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldIcons compareNewArray: (NSArray *)newIcons inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user
{
    for (NSDictionary *IconDictionary in newIcons) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((whoOwnIcon == %@) AND (idImage == %@))", user, [NSNumber numberWithInt:[IconDictionary[IMAGE_ID_IMG] integerValue]]];
        NSArray *results = [oldIcons filteredArrayUsingPredicate:predicate];
        if ([results count] != 0) {
            Icon *icon = [results lastObject];
            [oldIcons removeObject:icon];
        }
    }
    for (Icon *icon in oldIcons) {
        [context deleteObject:icon];
    }
}

@end
