//
//  Support.h
//  thesis
//
//  Created by horkavlna on 14/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAvailabity.h"

#define INTEGOO_FETCH_EVENT @"Integoo FATCH EVENT"
#define INTEGOO_FETCH_EXIST_ALERT @"Integoo FATCH EXIST ALERT"
#define INTEGOO_FETCH_NEW_ALERT @"Integoo FATCH NEW ALERT"
#define INTEGOO_FETCH_FILTER @"Integoo FATCH FILTER"
#define INTEGOO_FETCH_ICON @"Integoo FATCH ICON"
#define INTEGOO_FETCH @"INTEGOO_FETCH"

#define DEFAULT_URL_SERVER_ADDRESS @"http://integoo.apiary.io"
#define DEFAULT_LIMIT_FETCH 20

#define URL_NEW_ALERTS @"/api/new/alerts"
#define URL_EXIST_ALERTS @"/api/exist/alerts"
#define URL_EVENTS @"/api/events"
#define URL_ALERT_CONFIRM @"/api/new/alerts/"
#define URL_AUTHENTICATION @"/api/login"
#define URL_ALERTS_IMAGE @"/api/alerts/"
#define URL_EVENTS_IMAGE @"/api/events/"
#define URL_ICON @"/api/events/icons"
#define URL_IMAGE @"/imgs"
#define URL_FILTER @"/api/events/filter"

#define HEADER_AUTHORIZATION @"Authorization"
#define HEADER_ACCEPT @"Accept"
#define HEADER_CONTENT_TYPE @"Content-Type"
#define HEADER_LOCATION @"Location"
#define HEADER_X_CREATED @"X-From"
#define HEADER_X_LIMIT @"X-Limit"
#define HEADER_X_OFFSET @"X-Offset"
#define HEADER_X_FILTER @"X-Filter"

#define HEADER_ACCEPT_CONTENT_TYPE @"application/json"

#define HTTP_METHOD_POST @"POST"
#define HTTP_METHOD_GET @"GET"
#define HTTP_METHOD_DELETE @"DELETE"
#define HTTP_METHOD_PUT @"PUT"

#define TIME_INTERVAL_TIMER_FOREGRAUND 120
#define TIME_INTERVAL_BACKGROUND_REQUEST 20
#define TIME_INTERVAL_FOREGRAUND_REQUEST 30

#define NEW_ALERTS 0
#define EXIST_ALERTS 1
#define EVENTS 2
#define FILTERS 3
#define ICONS 4

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)


@interface Support : NSObject

+ (BOOL)saveDefaultUser: (NSString *) defaultUser;
+ (NSString *)getDefaultUser;

+ (BOOL)saveUser: (NSString *) userName withPassword: (NSString *) password;
+ (NSString *)getUsername;
+ (NSString *)getPassword;
+ (NSDictionary *)getUser;
+ (void)removeUser;

+ (void) showAlertTitle: (NSString *)title withMessage: (NSString *)message withDelegate: (id)delegate cancelButton: (NSString *) cancelButtonTitle otherButton:(NSString *) otherButtonTitle withIdentifier: (NSInteger) tagId;
+ (UIColor *) standardColorForSettingsCellTableView;
+ (void) wrongAuthentication: (NSString *) invalidData withLable: (UILabel *)invalidInputData;
+ (NSData*)blobToNSData: (NSArray*) blob;

+ (NSString *)getUsernameAndPasswordForBase64Encoded: (NSString *) userName withPassword: (NSString *) password;
+ (BOOL)saveDeviceToken: (NSString *) token;
+ (NSString *)getDeviceToken;
+ (BOOL)saveTimeAutomaticLogout: (NSTimeInterval) time;
+ (NSTimeInterval)getTimeAutomaticLogout;
+ (BOOL) saveTimeLogoutSwitcher:(BOOL) valueSwitcher;
+ (BOOL)getTimeLogoutSwitcher;
+ (BOOL) saveLastTimeUserInApplication;
+ (NSTimeInterval) getLastTimeUserInApplication;

+ (BOOL) registerForRemoteNotification;
+ (BOOL) unregisterForRemoteNotification;
+ (NSNumber *) getRegisterForRemoteNotification;

+ (BOOL)saveServerAddress: (NSString *) serverAddress;
+ (NSString *)getServerAddress;
+ (NSURL *)getWholeAddress: (NSString *)appendPostfixAddress;

+ (BOOL)saveLimitFetch: (NSNumber *) limitFetch;
+ (NSNumber *)getLimitFetch;

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert withAlpha:(float) alpha;

+ (NSArray *)getFetchLimitItem;

+ (NSString *) durationTime:(NSTimeInterval) seconds;
+ (NSDate *) parseJsonDateToNSDate: (NSString *) timeJson;

+ (CGRect)getScreenFrameForOrientation:(UIInterfaceOrientation)orientation;
+ (CGSize)sizeOfText:(NSString *)textToMesure widthOfTextView:(CGFloat)width withFont:(UIFont*)font;

@end
