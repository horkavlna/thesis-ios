//
//  User+Integoo.h
//  thesis
//
//  Created by horkavlna on 07/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "User.h"

#define USER_IDLOCATION @"idLocation"
#define USER_IMEI @"imei"
#define USER_USERNAME @"username"
#define USER_PASSWORD @"password"

@interface User (Integoo)

+ (void) saveUser: (NSDictionary *)userDictionary intoManagedObjectContext:(NSManagedObjectContext *)context;
+ (User *) fetchUser: (NSManagedObjectContext *)managedObjectContext withUserName: (NSString *) userName;
+ (void) removeUser: (NSManagedObjectContext *)managedObjectContext;

@end
