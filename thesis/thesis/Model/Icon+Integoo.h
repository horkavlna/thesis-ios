//
//  Icon+Integoo.h
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Icon.h"
#import "Support.h"

#define IMAGE_IMG @"img"
#define IMAGE_ID_IMG @"id"

@interface Icon (Integoo)

+ (void) saveIcon: (NSDictionary *)iconDictionary inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user;
+ (void) loadIconsFromIntegooArray: (NSArray *)iconArray intoManagedObjectContext:(NSManagedObjectContext *)context withUser: (User *) user;
+ (Icon *) findIcon: (NSManagedObjectContext *)context withIconId: (NSNumber *)idIcon withUser: (User *) user;
+ (BOOL) existIcon: (NSManagedObjectContext *)context withUser: (User *) user;
+ (NSMutableArray*) fetchAllIcon: (NSManagedObjectContext *)context withUser: (User *) user;
+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldIcons compareNewArray: (NSArray *)newIcons inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user;

@end
