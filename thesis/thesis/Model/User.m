//
//  User.m
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "User.h"
#import "Alert.h"
#import "Filter.h"


@implementation User

@dynamic idLocation;
@dynamic imei;
@dynamic password;
@dynamic username;
@dynamic alerts;
@dynamic filters;
@dynamic imageicon;

@end
