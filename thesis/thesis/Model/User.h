//
//  User.h
//  integoo
//
//  Created by horkavlna on 09/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Alert, Filter;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * idLocation;
@property (nonatomic, retain) NSString * imei;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *alerts;
@property (nonatomic, retain) NSSet *filters;
@property (nonatomic, retain) NSSet *imageicon;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addAlertsObject:(Alert *)value;
- (void)removeAlertsObject:(Alert *)value;
- (void)addAlerts:(NSSet *)values;
- (void)removeAlerts:(NSSet *)values;

- (void)addFiltersObject:(Filter *)value;
- (void)removeFiltersObject:(Filter *)value;
- (void)addFilters:(NSSet *)values;
- (void)removeFilters:(NSSet *)values;

- (void)addImageiconObject:(NSManagedObject *)value;
- (void)removeImageiconObject:(NSManagedObject *)value;
- (void)addImageicon:(NSSet *)values;
- (void)removeImageicon:(NSSet *)values;

@end
