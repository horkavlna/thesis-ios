//
//  Alert+Integoo.m
//  thesis
//
//  Created by horkavlna on 25/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Alert+Integoo.h"
#import "Alert.h"
#import "User+Integoo.h"
#import "Icon+Integoo.h"
#import "Support.h"

@implementation Alert (Integoo)

+ (void) saveAlerts: (NSDictionary *)alertDictionary inManagedObjectContext: (NSManagedObjectContext *)context withType:(NSNumber *) type withUser:(User *) user
{
    Alert *alert = nil;
    NSNumber *idIntegoo = alertDictionary[ALERT_IDINTEGOO];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Alert"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (idAlert == %@))", user, idIntegoo];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || (matches.count > 1)) {
    } else if ([matches count]) {
    } else {
        alert = [NSEntityDescription insertNewObjectForEntityForName:@"Alert" inManagedObjectContext:context];

        alert.idAlert = alertDictionary[ALERT_IDINTEGOO];
        alert.title = alertDictionary[ALERT_NAME];
        alert.device = alertDictionary[ALERT_DEVICE];
        alert.place = alertDictionary[ALERT_PLACE];
        alert.created = [Support parseJsonDateToNSDate:[NSString stringWithFormat:@"%@",alertDictionary[ALERT_CREATED]]];
        alert.color = alertDictionary[ALERT_COLOR];
        alert.instructions = alertDictionary[ALERT_INSTRUCTIONS];
        alert.confirmed = alertDictionary[ALERT_CONFIRMED];
        alert.type = type;
        alert.whoOwnAlert = user;
    }
}

+ (void) saveImage: (Alert *)alertOld addImage:(NSArray *)arrayImage intoManagedObjectContext:(NSManagedObjectContext *)context
{
    Alert *alert = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Alert"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (idAlert == %@))", alertOld.whoOwnAlert, alertOld.idAlert];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (error != nil) {
    }
    else if (matches.count == 1) {
        alert = [matches lastObject];
        alert.img = [Support blobToNSData:[[NSMutableArray alloc] initWithArray:[arrayImage valueForKey:IMAGE_IMG]]];
    }
}

+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldAlerts compareNewArray: (NSMutableArray *)newArray
              inManagedObjectContext: (NSManagedObjectContext *)context withType:(NSNumber *)type withUser: (User *) user
{
    NSLog(@"%@ newArray:%d", [[Alert class] description], newArray.count);
    NSLog(@"%@ oldArray:%d", [[Alert class] description], oldAlerts.count);
    for (NSDictionary *alertDictionary in newArray) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (idAlert == %@) AND (type == %@))", user,
                                  [NSNumber numberWithInt:[alertDictionary[ALERT_IDINTEGOO] integerValue]], type];
        NSArray *results = [oldAlerts filteredArrayUsingPredicate:predicate];
        if ([results count] != 0) {
            Alert *alert = [results firstObject];
            [oldAlerts removeObject:alert];
        }
    }
    for (Alert *alert in oldAlerts) {
        [context deleteObject:alert];
    }
}

+ (NSMutableArray*) fetchAllAlerts: (NSManagedObjectContext *)context withType:(NSNumber *)type withUser: (User *) user
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Alert"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (type == %@))", user, type];
    NSError *error;
    NSMutableArray *matches = [NSMutableArray arrayWithArray: [context executeFetchRequest:request error:&error]];
    if (!matches || error ) {
        NSLog(@"%@ error deleteAllAlerts %@ matches %d", [[Alert class] description], error, [matches count]);
        return nil;
    } else {
        NSLog(@"%@ fetchAllAlerts count %d", [[Alert class] description], [matches count]);
        return matches;
    }
}

+ (Alert *) findAlertFromRemotePushNotification: (NSManagedObjectContext *)context
                                    withAlertId: (NSNumber *)idAlert withType:(NSNumber *)type withUser: (User *) user
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Alert"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (idAlert == %@) AND (type == %@))", user,  idAlert, type];
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    if (!matches || error || matches.count == 0 ) {
        NSLog(@"%@ error findAlertFromRemotePushNotification alert doesn't exist", [[Alert class] description]);
        return nil;
    } else {
        NSLog(@"%@ findAlertFromRemotePushNotification finded object %@", [[Alert class] description], [matches firstObject]);
        return [matches firstObject];
    }
}

+ (void) loadAlertsFromIntegooArray: (NSArray *)newAlertArray intoManagedObjectContext:(NSManagedObjectContext *)context
                           withType:(NSNumber *)type withUser:(User *)user
{
    for (NSDictionary *alertDictionary in newAlertArray) {
        [self saveAlerts:alertDictionary inManagedObjectContext:context withType:type withUser:user];
    }
}

+ (void) deleteAlertPressConfirmButton: (Alert *)alert intoManagedObjectContext:(NSManagedObjectContext *)context
{
    NSLog(@"%@ deleteAlertPressConfirmButton", [[Alert class] description]);
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Alert"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (idAlert == %@))", alert.whoOwnAlert, alert.idAlert];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    if (matches.count == 1) {
        Alert *alertCoreData = [matches lastObject];
        [context deleteObject:alertCoreData];
    } else {
        NSLog(@"%@ can't delete object are mouch or doesn't exist", [[Alert class] description]);
    }
}

@end
