//
//  Filter+Integoo.m
//  thesis
//
//  Created by horkavlna on 18/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "Filter+Integoo.h"
#import "User+Integoo.h"
#import "Support.h"

@implementation Filter (Integoo)

+ (void) saveFilter: (NSDictionary *)filterDictionary inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user
{
    Filter *filter = nil;
    NSNumber *idIntegoo = filterDictionary[FILTER_IDINTEGOO];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Filter"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnFilter == %@) AND (idFilter == %@))", user, idIntegoo];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || (matches.count > 1)) {
    } else if ([matches count]) {
    } else {
        filter = [NSEntityDescription insertNewObjectForEntityForName:@"Filter" inManagedObjectContext:context];
        filter.idFilter = filterDictionary[FILTER_IDINTEGOO];
        filter.name = filterDictionary[FILTER_NAME];
        filter.isChecked = [NSNumber numberWithInt:0];
        filter.whoOwnFilter = user;
    }
}

+ (void) loadFiltersFromIntegooArray: (NSArray *)filterArray intoManagedObjectContext:(NSManagedObjectContext *)context withUser: (User *) user
{
    for (NSDictionary *filterDictionary in filterArray) {
        [self saveFilter:filterDictionary inManagedObjectContext:context withUser:user];
    }
}

+ (NSMutableArray*) fetchAllFilters: (NSManagedObjectContext *)context withUser: (User *) user
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Filter"];
    request.predicate = [NSPredicate predicateWithFormat:@"whoOwnFilter == %@", user];
    NSError *error;
    NSMutableArray *matches = [NSMutableArray arrayWithArray: [context executeFetchRequest:request error:&error]];
    if (!matches || error ) {
        NSLog(@"%@ error deleteAllFilters %@ matches %d", [[Filter class] description], error, [matches count]);
        return nil;
    } else {
        NSLog(@"%@ fetchAllFilters count %d", [[Filter class] description], [matches count]);
        return matches;
    }
}

+ (void) deleteOldObjectsFromNSArray: (NSMutableArray *)oldFilters compareNewArray: (NSArray *)newFilters inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user
{
    for (NSDictionary *filterDictionary in newFilters) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((whoOwnFilter == %@) AND (idFilter == %@))", user, [NSNumber numberWithInt:[filterDictionary[FILTER_IDINTEGOO] integerValue]]];
        NSArray *results = [oldFilters filteredArrayUsingPredicate:predicate];
        if ([results count] != 0) {
            Filter *filter = [results lastObject];
            [oldFilters removeObject:filter];
        }
    }
    for (Filter *filter in oldFilters) {
        [context deleteObject:filter];
    }
}

+ (Filter*) fetchFilter:(NSNumber *)idFilter inManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user
{
    Filter *filter = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Filter"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnFilter == %@) AND (idFilter == %@))", user, idFilter];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    if ([matches count] != 0) {
        filter = [matches lastObject];
    }
    NSLog(@"%@ fetchFilter %@", [[Filter class] description], filter.name);
    return filter;
}

+ (Filter*) fetchSelectedFilterManagedObjectContext: (NSManagedObjectContext *)context withUser: (User *) user
{
    Filter *filter = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Filter"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnFilter == %@) AND (isChecked == 1))", user];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    if ([matches count] != 0) {
        filter = [matches lastObject];
    }
    NSLog(@"%@ fetchFilter %@", [[Filter class] description], filter.name);
    return filter;
}

@end
