//
//  User+Integoo.m
//  thesis
//
//  Created by horkavlna on 07/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "User+Integoo.h"
#import "User.h"

@implementation User (Integoo)

+ (void) saveUser: (NSDictionary *)userDictionary intoManagedObjectContext:(NSManagedObjectContext *)context
{
    User *user = nil;
    NSString *username = userDictionary[USER_USERNAME];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"username MATCHES %@", username];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || (matches.count > 1)) {
        NSLog(@"%@ user exist in database %@", [[User class] description], user.username);
    } else if ([matches count]) {
    } else {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        user.idLocation = userDictionary[USER_IDLOCATION];
        user.imei = userDictionary[USER_IMEI];
        user.username = userDictionary[USER_USERNAME];
        user.password = userDictionary[USER_PASSWORD];
    }
}

+ (User *) fetchUser: (NSManagedObjectContext *)managedObjectContext withUserName: (NSString *) userName
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"username MATCHES %@", userName];
    NSError *error;
    NSArray *matches = [managedObjectContext executeFetchRequest:request error:&error];
    if (!matches || error || matches.count == 0 ) {
        NSLog(@"%@ error fetchUser user %@ doesn't exist", [[User class] description], userName);
        return nil;
    } else {
        NSLog(@"%@ fetchUser user %@", [[User class] description], [matches firstObject]);
        return [matches firstObject];
    }
}

+ (void) removeUser: (NSManagedObjectContext *)managedObjectContext
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    NSError *error;
    User *user = [[managedObjectContext executeFetchRequest:request error:&error] lastObject];
    NSLog(@"%@ remove user %@", [[User class] description], user);
    if (user != nil) {
        [managedObjectContext deleteObject:user];
    }
}

@end
