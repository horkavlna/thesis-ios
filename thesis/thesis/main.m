                                                                               //
//  main.m
//  thesis
//
//  Created by horkavlna on 13/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ThesisDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ThesisDelegate class]));
    }
}
