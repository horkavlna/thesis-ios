//
//  AppDelegate.m
//  thesis
//
//  Created by horkavlna on 13/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "ThesisDelegate.h"
#import "Support.h"
#import "ThesisDelegate+MOC.h"
#import "Event+Integoo.h"
#import "DatabaseAvailabity.h"
#import "Alert+Integoo.h"
#import "NewAlertsCDTVC.h"
#import "User+Integoo.h"
#import "SigninViewController.h"
#import "MenuViewController.h"
#import "Filter+Integoo.h"
#import "Icon+Integoo.h"
#import "Controller/ConfigTableViewController.h"

@interface ThesisDelegate() <NSURLSessionDownloadDelegate>

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (copy, nonatomic) void (^integooDownloadBackgroundURLSessionCompletionHandler)();
@property (strong, nonatomic) NSTimer *integooForegroundFetchTimer;
@property (strong, nonatomic) NSURLSession *integooDownloadSession;

//EVENTS
@property (strong, nonatomic) NSArray *oldArrayEvents;
@property (strong, nonatomic) NSMutableArray *arrayNewEvents;
//NEW ALERTS
@property (strong, nonatomic) NSArray *oldArrayNewAlerts;
@property (strong, nonatomic) NSMutableArray *arrayNewAlerts;
//EXIST ALERTS
@property (strong, nonatomic) NSArray *oldArrayExistAlerts;
@property (strong, nonatomic) NSMutableArray *arrayExistAlerts;
//FILTERS
@property (strong, nonatomic) NSArray *oldFilters;
//FILTERS
@property (strong, nonatomic) NSArray *oldIcons;

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) Filter *selectedFilter;
@property (strong, nonatomic) NSString *nameClass;

@end

@implementation ThesisDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    UIImage *image = [UIImage imageNamed:@"Devices"];
//    NSData *data = UIImagePNGRepresentation(image);
//    NSUInteger len = data.length;
//    uint8_t *bytes = (uint8_t *)[data bytes];
//    NSMutableString *result = [NSMutableString stringWithCapacity:len * 3];
//    [result appendString:@"["];
//    for (NSUInteger i = 0; i < len; i++) {
//        if (i) {
//            [result appendString:@","];
//        }
//        [result appendFormat:@"%d", bytes[i]];
//    }
//    [result appendString:@"]"];
//    NSLog(@"img %@",result);
    self.nameClass = [[ThesisDelegate class] description];

    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    if ([[Support getRegisterForRemoteNotification] intValue] == 0) {
        [Support registerForRemoteNotification];
    }
    if ([Support getServerAddress] == nil) {
        [Support saveServerAddress:DEFAULT_URL_SERVER_ADDRESS];
    }
    if ([Support getLimitFetch] == nil) {
        [Support saveLimitFetch:[NSNumber numberWithInt:DEFAULT_LIMIT_FETCH]];
    }
    self.managedObjectContext = [self createMainQueueManagedObjectContext];
    [self startIntegooFetch];
    
    return YES;
}

- (void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    NSLog(@"%@ managedObjectContext %@",self.nameClass, _managedObjectContext);
    _managedObjectContext = managedObjectContext;
    
    NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext, Logout : @FALSE} : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:SigninDatabaseAvailabityNotification object:self userInfo:userInfo];
    
    [self.integooForegroundFetchTimer invalidate];
    self.integooForegroundFetchTimer = nil;
    if (self.managedObjectContext) {
        self.integooForegroundFetchTimer = [NSTimer scheduledTimerWithTimeInterval:TIME_INTERVAL_TIMER_FOREGRAUND target:self selector:@selector(startIntegooFetch:) userInfo:nil repeats:YES];
    }
    // let everyone who might be interested know this context is available
    // this happens very early in the running of our application
    // it would make NO SENSE to listen to this radio station in a View Controller that was segued to, for example
    // (but that's okay because a segued-to View Controller would presumably be "prepared" by being given a context to work in)
}

// this is called occasionally by the system WHEN WE ARE NOT THE FOREGROUND APPLICATION
- (void) application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"%@ performFetchWithCompletionHandler WHEN WE ARE NOT THE FOREGROUND APPLICATION", self.nameClass);
    [self startIntegooFetch];
    if (self.managedObjectContext) {
        NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
        if (base64Encoded != nil) {
            NSMutableArray *alertArray = [[NSMutableArray alloc] init];
            self.user = [User fetchUser:self.managedObjectContext withUserName:[Support getUsername]];
            [self.managedObjectContext performBlock:^{
                [alertArray addObjectsFromArray:[Alert fetchAllAlerts:self.managedObjectContext withType:[NSNumber numberWithInt:NEW_ALERTS]
                                                             withUser:self.user]];
                [self.managedObjectContext save:NULL];
            }];
            [self backgroundFetch:completionHandler withBase: base64Encoded lastCreated:0 withOffset:0
                    withOldAlerts:alertArray
                      wihNewArray:[[NSMutableArray alloc] init] withAddress:[Support getWholeAddress:URL_NEW_ALERTS]];
            
        } else {
            completionHandler(UIBackgroundFetchResultNoData);
        }
    } else {
         completionHandler(UIBackgroundFetchResultNoData);
    }
}

- (void) backgroundFetch:(void (^)(UIBackgroundFetchResult))completionHandlerBackground withBase:(NSString *) base64Encoded lastCreated: (NSTimeInterval)created withOffset:(NSUInteger)offset withOldAlerts: (NSArray*)oldArray wihNewArray: (NSMutableArray *) newArray withAddress: (NSURL *) address
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:address cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
    [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
    [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
    [request addValue:[NSString stringWithFormat: @"%f", created] forHTTPHeaderField:HEADER_X_CREATED];
    [request addValue:[NSString stringWithFormat: @"%@", [Support getLimitFetch]] forHTTPHeaderField:HEADER_X_LIMIT];
    [request addValue:[NSString stringWithFormat: @"%d", (int)offset] forHTTPHeaderField:HEADER_X_OFFSET];
    [request setHTTPMethod:HTTP_METHOD_GET];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.timeoutIntervalForRequest = TIME_INTERVAL_BACKGROUND_REQUEST;
    configuration.allowsCellularAccess = NO;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDownloadTask *downloadTaskEvents = [session downloadTaskWithRequest:request
          completionHandler:^(NSURL *url, NSURLResponse *response, NSError *error) {
              if (!error) {
                  if ([request.URL isEqual:[Support getWholeAddress:URL_NEW_ALERTS]]) {
                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                      if ([httpResponse statusCode] == 200) {
                          NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                          NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                          NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                          NSArray *newPartArray = [self putIntoArrayFromJson:url intoContext:self.managedObjectContext withType:NEW_ALERTS  andThenExecuteBlock:nil];
                          [newArray addObjectsFromArray: newPartArray];
                          NSLog(@"%@ whole new alerts count %lu", self.nameClass, (unsigned long)newArray.count);
                          if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
                              [self backgroundFetch:completionHandlerBackground withBase:base64Encoded lastCreated:created withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue] withOldAlerts:oldArray wihNewArray:newArray
                               withAddress:address];
                          } else {
                              [self.managedObjectContext performBlock:^{
                                    [Alert deleteOldObjectsFromNSArray:[oldArray mutableCopy]  compareNewArray:newArray inManagedObjectContext:self.managedObjectContext withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
                                  [self.managedObjectContext save:NULL];
                              }];
                              NSLog(@"%@ completionHandlerBackground UIBackgroundFetchResultNewData NEW ALERTS", self.nameClass);
                             // completionHandlerBackground(UIBackgroundFetchResultNewData);
                              NSMutableArray *fetchAllAlerts = [[NSMutableArray alloc] init];
                              [self.managedObjectContext performBlock:^{
                                  [fetchAllAlerts addObjectsFromArray:[Alert fetchAllAlerts:self.managedObjectContext withType:[NSNumber numberWithInt:EXIST_ALERTS] withUser:self.user]];
                                  [self.managedObjectContext save:NULL];
                              }];
                              [self backgroundFetch:completionHandlerBackground withBase:base64Encoded lastCreated:0 withOffset:0 withOldAlerts:fetchAllAlerts wihNewArray:[[NSMutableArray alloc] init] withAddress:[Support getWholeAddress:URL_EXIST_ALERTS]];
                          }
                      }
                  } else if ([request.URL isEqual:[Support getWholeAddress:URL_EXIST_ALERTS]]) {
                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                      if ([httpResponse statusCode] == 200) {
                          NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                          NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                          NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                          NSArray *newPartArray = [self putIntoArrayFromJson:url intoContext:self.managedObjectContext withType:EXIST_ALERTS andThenExecuteBlock:nil];
                          [newArray addObjectsFromArray: newPartArray];
                          NSLog(@"%@ whole exist alerts count %lu", self.nameClass, (unsigned long)newArray.count);
                          if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
                              [self backgroundFetch:completionHandlerBackground withBase:base64Encoded lastCreated:created withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue] withOldAlerts:oldArray wihNewArray:newArray
                                        withAddress:address];
                          } else {
                              [self.managedObjectContext performBlock:^{
                                    [Alert deleteOldObjectsFromNSArray:[oldArray mutableCopy]  compareNewArray:newArray inManagedObjectContext:self.managedObjectContext withType:[NSNumber numberWithInt:EXIST_ALERTS] withUser:self.user];
                                  [self.managedObjectContext save:NULL];
                              }];
                              NSLog(@"%@ completionHandlerBackground UIBackgroundFetchResultNewData EXIST ALERTS", self.nameClass);
                              //completionHandlerBackground(UIBackgroundFetchResultNewData);
                              self.selectedFilter= [Filter fetchSelectedFilterManagedObjectContext:self.managedObjectContext withUser:self.user];
                              if (self.selectedFilter != nil) {
                                  NSMutableArray *fetchAllEvents = [[NSMutableArray alloc] init];
                                  [self.managedObjectContext performBlock:^{
                                      [fetchAllEvents addObjectsFromArray:[Event fetchAllEvents:self.managedObjectContext withFilter:self.selectedFilter]];
                                      [self.managedObjectContext save:NULL];
                                  }];
                                  [self backgroundFetch:completionHandlerBackground withBase:base64Encoded lastCreated:0 withOffset:0 withOldAlerts:fetchAllEvents wihNewArray:[[NSMutableArray alloc] init] withAddress:[Support getWholeAddress:URL_EVENTS]];
                              } else {
                                  completionHandlerBackground(UIBackgroundFetchResultNewData);
                              }
                          }
                      }
                  } else if ([request.URL isEqual:[Support getWholeAddress:URL_EVENTS]]) {
                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                          if ([httpResponse statusCode] == 200) {
                              NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                              NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                              NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                              NSArray *newPartArray = [self putIntoArrayFromJson:url intoContext:self.managedObjectContext withType:EVENTS andThenExecuteBlock:nil];
                              [newArray addObjectsFromArray: newPartArray];
                              NSLog(@"%@ whole events count %lu", self.nameClass, (unsigned long)newArray.count);
                              if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
                                  [self backgroundFetch:completionHandlerBackground withBase:base64Encoded lastCreated:created withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue] withOldAlerts:oldArray wihNewArray:newArray withAddress:address];
                              } else {
                                  NSLog(@"%@ completionHandlerBackground UIBackgroundFetchResultNewData EVENTS", self.nameClass);
                                  [self.managedObjectContext performBlock:^{
                                        [Event deleteOldObjectsFromNSArray:[oldArray mutableCopy]  compareNewArray:newArray inManagedObjectContext:self.managedObjectContext withFilter:self.selectedFilter];
                                      [self.managedObjectContext save:NULL];
                                  }];
                                  completionHandlerBackground(UIBackgroundFetchResultNewData);
                              }
                          }
                  } else {
                          NSLog(@"%@ ERROR - bad request: %@", self.nameClass, error);
                          completionHandlerBackground(UIBackgroundFetchResultNoData);
                      }
              } else {
                  NSLog(@"%@ ERROR - bad request: %@", self.nameClass, error);
                  completionHandlerBackground(UIBackgroundFetchResultNoData);
              }
          }];
    [downloadTaskEvents resume];
}

// this is called whenever a URL we have requested with a background session returns and we are in the background
// it is essentially waking us up to handle it
// if we were in the foreground iOS would just call our delegate method and not bother with this
- (void) application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    self.integooDownloadBackgroundURLSessionCompletionHandler = completionHandler;
}


// not required by the protocol, but we should definitely catch errors here
// so that we can avoid crashes
// and also so that we can detect that download tasks are (might be) complete
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error && (session == self.integooDownloadSession)) {
        NSLog(@"%@ Integoo background download session failed: %@", self.nameClass, error.localizedDescription);
        [self integooDownloadTasksMightBeComplete];
    }
}

// this is "might" in case some day we have multiple downloads going on at once
- (void)integooDownloadTasksMightBeComplete
{
    if (self.integooDownloadBackgroundURLSessionCompletionHandler) {
        [self.integooDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
            // we're doing this check for other downloads just to be theoretically "correct"
            //  but we don't actually need it (since we only ever fire off one download task at a time)
            // in addition, note that getTasksWithCompletionHandler: is ASYNCHRONOUS
            //  so we must check again when the block executes if the handler is still not nil
            //  (another thread might have sent it already in a multiple-tasks-at-once implementation)
            if (![downloadTasks count]) {  // any more Flickr downloads left?
                // nope, then invoke flickrDownloadBackgroundURLSessionCompletionHandler (if it's still not nil)
                void (^completionHandler)() = self.integooDownloadBackgroundURLSessionCompletionHandler;
                self.integooDownloadBackgroundURLSessionCompletionHandler = nil;
                if (completionHandler) {
                    completionHandler();
                }
            } // else other downloads going, so let them call this method when they finish
        }];
    }
}

- (NSArray *)integooAtURL:(NSURL *)data
{
    NSArray *entity;
    NSData *integooJSONData = [NSData dataWithContentsOfURL:data];  // will block if url is not local!
    if (integooJSONData) {
        NSError *error;
        entity = [NSJSONSerialization JSONObjectWithData:integooJSONData
                                                             options:0
                                                               error:&error];
    }
    return entity;
}

- (NSArray *)putIntoArrayFromJson:(NSURL *)url intoContext:(NSManagedObjectContext *)context withType:(NSInteger) type andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *arrayObjects = [self integooAtURL:url];
        NSLog(@"%@ putIntoCoreData events %lu and type %ld", self.nameClass, (unsigned long)arrayObjects.count, (long)type);
        [context performBlock:^{
            if (type == NEW_ALERTS) {
                [Alert loadAlertsFromIntegooArray:arrayObjects intoManagedObjectContext:context withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
            } else if (type == EXIST_ALERTS) {
                [Alert loadAlertsFromIntegooArray:arrayObjects intoManagedObjectContext:context withType:[NSNumber numberWithInt:EXIST_ALERTS] withUser:self.user];
            } else if (type == EVENTS) {
                [Event loadEventsFromIntegooArray:arrayObjects intoManagedObjectContext:context withFilter:self.selectedFilter withUser:self.user];
            } else if (type == FILTERS) {
                [Filter loadFiltersFromIntegooArray:arrayObjects intoManagedObjectContext:context withUser:self.user];
            }  else if (type == ICONS) {
                [Icon loadIconsFromIntegooArray:arrayObjects intoManagedObjectContext:context withUser:self.user];
            }
            [context save:NULL];
            if (whenDone) whenDone();
        }];
        return arrayObjects;
    } else {
        if (whenDone) whenDone();
    }
    return nil;
}


- (void) startIntegooFetch
{
    NSLog(@"%@ performFetchWithCompletionHandler WHEN WE ARE THE FOREGROUND APPLICATION", self.nameClass);
    if (self.managedObjectContext) {
        NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
        if (base64Encoded != nil) {
            [self.managedObjectContext performBlock:^{
                self.user = [User fetchUser:self.managedObjectContext withUserName:[Support getUsername]];
                self.oldArrayNewAlerts = [Alert fetchAllAlerts:self.managedObjectContext withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
                [self.managedObjectContext save:NULL];
            }];
            self.arrayNewAlerts = [[NSMutableArray alloc]init];
            [self startSesstionDownloadNewAlerts: base64Encoded withOffset:0];
            
            [self.managedObjectContext performBlock:^{
                self.oldArrayExistAlerts = [Alert fetchAllAlerts:self.managedObjectContext withType:[NSNumber numberWithInt:EXIST_ALERTS] withUser:self.user];
                [self.managedObjectContext save:NULL];
            }];
            self.arrayExistAlerts = [[NSMutableArray alloc]init];
            [self startSesstionDownloadExistAlerts: base64Encoded withOffset:0];
            self.selectedFilter= [Filter fetchSelectedFilterManagedObjectContext:self.managedObjectContext withUser:self.user];
            if (self.selectedFilter != nil) {
                //call Events
                [self.managedObjectContext performBlock:^{
                    self.oldArrayEvents = [Event fetchAllEvents:self.managedObjectContext withFilter:self.selectedFilter];
                    [self.managedObjectContext save:NULL];
                }];
                self.arrayNewEvents = [[NSMutableArray alloc]init];
                [self startSesstionDownloadEvents:(NSString *) base64Encoded withOffset:0];
            } else {
                [self.managedObjectContext performBlock:^{
                    self.oldFilters = [Filter fetchAllFilters:self.managedObjectContext withUser:self.user];
                    [self.managedObjectContext save:NULL];
                }];
                [self startSesstionDownloadFilters:(NSString *) base64Encoded];
                [self.managedObjectContext performBlock:^{
                    self.oldIcons = [Icon fetchAllIcon:self.managedObjectContext withUser:self.user];
                    [self.managedObjectContext save:NULL];
                }];
                [self startSesstionDownloadIcon:(NSString *) base64Encoded];
            }
        }
    }
}

- (void) startIntegooFetch:(NSTimer *)timer
{
    [self startIntegooFetch];
}

- (void)startSesstionDownloadIcon:(NSString *) base64Encoded
{
    NSLog(@"%@ startSesstionDownloadIcon", self.nameClass);
    [self.integooDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        // let's see if we're already working on a fetch ...
        if (![downloadTasks count]) {
            [NSThread sleepForTimeInterval:0.01];
            // ... not working on a fetch, let's start one up
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_ICON] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
            [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
            [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
            [request setHTTPMethod:HTTP_METHOD_GET];
            
            NSURLSessionDownloadTask *taskEvent = [self.integooDownloadSession downloadTaskWithRequest:request];
            taskEvent.taskDescription = INTEGOO_FETCH_ICON;
            [taskEvent resume];
        } else {
            NSLog(@"%@ startSesstionDownloadIcon - we are working on a fetch", self.nameClass);
            // ... we are working on a fetch (let's make sure it (they) is (are) running while we're here)
            for (NSURLSessionDownloadTask *task in downloadTasks) {
                [task resume];
            }
        }
    }];
}

- (void)startSesstionDownloadFilters:(NSString *) base64Encoded
{
    NSLog(@"%@ startSesstionDownloadFilters", self.nameClass);
    [self.integooDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        // let's see if we're already working on a fetch ...
        if (![downloadTasks count]) {
            [NSThread sleepForTimeInterval:0.01];
            // ... not working on a fetch, let's start one up
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_FILTER] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
            [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
            [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
            [request setHTTPMethod:HTTP_METHOD_GET];
            
            NSURLSessionDownloadTask *taskEvent = [self.integooDownloadSession downloadTaskWithRequest:request];
            taskEvent.taskDescription = INTEGOO_FETCH_FILTER;
            [taskEvent resume];
        } else {
            NSLog(@"%@ startSesstionDownloadFilters - we are working on a fetch", self.nameClass);
            // ... we are working on a fetch (let's make sure it (they) is (are) running while we're here)
            for (NSURLSessionDownloadTask *task in downloadTasks) {
                [task resume];
            }
        }
    }];
}

- (void)startSesstionDownloadNewAlerts:(NSString *) base64Encoded withOffset:(NSUInteger)offset
{
    NSLog(@"%@ startSesstionDownloadNewAlerts", self.nameClass);
    [self.integooDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        // let's see if we're already working on a fetch ...
        if (![downloadTasks count]) {
            [NSThread sleepForTimeInterval:0.01];
            // ... not working on a fetch, let's start one up
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_NEW_ALERTS] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
            [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
            [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
            [request addValue:[NSString stringWithFormat: @"%@", [Support getLimitFetch]] forHTTPHeaderField:HEADER_X_LIMIT];
            [request addValue:[NSString stringWithFormat: @"%d", (int)offset] forHTTPHeaderField:HEADER_X_OFFSET];
            [request setHTTPMethod:HTTP_METHOD_GET];
            
            NSURLSessionDownloadTask *taskEvent = [self.integooDownloadSession downloadTaskWithRequest:request];
            taskEvent.taskDescription = INTEGOO_FETCH_NEW_ALERT;
            [taskEvent resume];
        } else {
            NSLog(@"%@ startSesstionDownloadNewAlerts - we are working on a fetch", self.nameClass);
            // ... we are working on a fetch (let's make sure it (they) is (are) running while we're here)
            for (NSURLSessionDownloadTask *task in downloadTasks) {
                [task resume];
            }
        }
    }];
}


- (void)startSesstionDownloadExistAlerts:(NSString *) base64Encoded withOffset:(NSUInteger)offset
{
    NSLog(@"%@ startSesstionDownloadExistAlerts", self.nameClass);
    [self.integooDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        // let's see if we're already working on a fetch ...
        if (![downloadTasks count]) {
            [NSThread sleepForTimeInterval:0.01];
            // ... not working on a fetch, let's start one up
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_EXIST_ALERTS] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
            [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
            [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
            [request addValue:[NSString stringWithFormat: @"%@", [Support getLimitFetch]] forHTTPHeaderField:HEADER_X_LIMIT];
            [request addValue:[NSString stringWithFormat: @"%d", (int)offset] forHTTPHeaderField:HEADER_X_OFFSET];
            [request setHTTPMethod:HTTP_METHOD_GET];
            
            NSURLSessionDownloadTask *taskEvent = [self.integooDownloadSession downloadTaskWithRequest:request];
            taskEvent.taskDescription = INTEGOO_FETCH_EXIST_ALERT;
            [taskEvent resume];
        } else {
            NSLog(@"%@ startSesstionDownloadExistAlerts - we are working on a fetch", self.nameClass);
            // ... we are working on a fetch (let's make sure it (they) is (are) running while we're here)
            for (NSURLSessionDownloadTask *task in downloadTasks) {
                [task resume];
            }
        }
    }];
}

// it will always work when we are the foreground (active) application, no we will be in background
- (void)startSesstionDownloadEvents:(NSString *) base64Encoded withOffset:(NSUInteger)offset
{
    NSLog(@"%@ startSesstionDownloadEvents", self.nameClass);
    [self.integooDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
            // let's see if we're already working on a fetch ...
            if (![downloadTasks count]) {
                [NSThread sleepForTimeInterval:0.01];
                // ... not working on a fetch, let's start one up
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_EVENTS] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
                [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
                [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
//                [request addValue:[NSString stringWithFormat: @"%@", [Support getFilterId]] forHTTPHeaderField:HEADER_X_FILTER];
                [request addValue:[NSString stringWithFormat: @"%@", [Support getLimitFetch]] forHTTPHeaderField:HEADER_X_LIMIT];
                [request addValue:[NSString stringWithFormat: @"%d", (int)offset] forHTTPHeaderField:HEADER_X_OFFSET];
                [request setHTTPMethod:HTTP_METHOD_GET];
                
                NSURLSessionDownloadTask *taskEvent = [self.integooDownloadSession downloadTaskWithRequest:request];
                taskEvent.taskDescription = INTEGOO_FETCH_EVENT;
                [taskEvent resume];
            } else {
                NSLog(@"%@ startSesstionDownloadEvents - we are working on a fetch", self.nameClass);
                // ... we are working on a fetch (let's make sure it (they) is (are) running while we're here)
                for (NSURLSessionDownloadTask *task in downloadTasks) {
                    [task resume];
                }
            }
        }];
}

- (NSURLSession *) integooDownloadSession
{
    if (!_integooDownloadSession) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken,^{
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfiguration:INTEGOO_FETCH_EVENT];
            configuration.timeoutIntervalForRequest = TIME_INTERVAL_FOREGRAUND_REQUEST;
            
            _integooDownloadSession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        });
        NSLog(@"%@ integooDownloadSession %@", self.nameClass, _integooDownloadSession);
    }
    return _integooDownloadSession;
}

// required by the protocol
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)localFile
{
    NSLog(@"%@ URLSession downloadTask", self.nameClass);
    if ([downloadTask.taskDescription isEqualToString:INTEGOO_FETCH_EVENT]) {
        NSLog(@"%@ didFinishDownloadingToURL in foregraund EVENT", self.nameClass);
        NSURLRequest *request = [downloadTask currentRequest];
        NSLog(@"%@ request events %@", self.nameClass, [request allHTTPHeaderFields]);
        NSArray *newPartArray = [self putIntoArrayFromJson:localFile intoContext:self.managedObjectContext withType:EVENTS andThenExecuteBlock:nil];
        [self.arrayNewEvents addObjectsFromArray: newPartArray];
        NSLog(@"%@ whole events count %lu", self.nameClass, (unsigned long)self.arrayNewEvents.count);
        if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
            NSLog(@"%@ OFFSET:%d", self.nameClass, [[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] +
                  [[Support getLimitFetch] integerValue]);
            [self startSesstionDownloadEvents:[[request allHTTPHeaderFields] valueForKey:HEADER_AUTHORIZATION] withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue]];
        } else {
            [self.managedObjectContext performBlock:^{
                [Event deleteOldObjectsFromNSArray:[self.oldArrayEvents mutableCopy]  compareNewArray:self.arrayNewEvents inManagedObjectContext:self.managedObjectContext withFilter:self.selectedFilter];
                [self.managedObjectContext save:NULL];
            }];
        }
    } else if ([downloadTask.taskDescription isEqualToString:INTEGOO_FETCH_NEW_ALERT]) {
        NSLog(@"%@ didFinishDownloadingToURL in foregraund NEW ALERT", self.nameClass);
        NSURLRequest *request = [downloadTask currentRequest];
        NSLog(@"%@ request new alert %@", self.nameClass, [request allHTTPHeaderFields]);
        NSArray *newPartArray = [self putIntoArrayFromJson:localFile intoContext:self.managedObjectContext withType:NEW_ALERTS andThenExecuteBlock:nil];
        [self.arrayNewAlerts addObjectsFromArray: newPartArray];
        NSLog(@"%@ whole new alert count %lu", self.nameClass, (unsigned long)self.arrayNewEvents.count);
        if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
            NSLog(@"%@ OFFSET:%d", self.nameClass, [[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] +
                  [[Support getLimitFetch] integerValue]);
            [self startSesstionDownloadNewAlerts:[[request allHTTPHeaderFields] valueForKey:HEADER_AUTHORIZATION] withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue]];
        } else {
            [self.managedObjectContext performBlock:^{
                [Alert deleteOldObjectsFromNSArray:[self.oldArrayNewAlerts mutableCopy]  compareNewArray:self.arrayNewAlerts inManagedObjectContext:self.managedObjectContext withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
                [self.managedObjectContext save:NULL];
            }];
        }
    } else if ([downloadTask.taskDescription isEqualToString:INTEGOO_FETCH_EXIST_ALERT]) {
        NSLog(@"%@ didFinishDownloadingToURL in foregraund EXIST ALERT", self.nameClass);
        NSURLRequest *request = [downloadTask currentRequest];
        NSLog(@"%@ request exist alert %@", self.nameClass, [request allHTTPHeaderFields]);
        NSArray *newPartArray = [self putIntoArrayFromJson:localFile intoContext:self.managedObjectContext withType:EXIST_ALERTS andThenExecuteBlock:nil];
        [self.arrayExistAlerts addObjectsFromArray: newPartArray];
        NSLog(@"%@ whole exist alert count %lu", self.nameClass, (unsigned long)self.arrayNewEvents.count);
        if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
            NSLog(@"%@ OFFSET:%d", self.nameClass, [[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] +
                  [[Support getLimitFetch] integerValue]);
            [self startSesstionDownloadExistAlerts:[[request allHTTPHeaderFields] valueForKey:HEADER_AUTHORIZATION] withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue]];
        } else {
            [self.managedObjectContext performBlock:^{
                [Alert deleteOldObjectsFromNSArray:[self.oldArrayExistAlerts mutableCopy]  compareNewArray:self.arrayExistAlerts inManagedObjectContext:self.managedObjectContext withType:[NSNumber numberWithInt:EXIST_ALERTS] withUser:self.user];
                [self.managedObjectContext save:NULL];
            }];
        }
    } else if ([downloadTask.taskDescription isEqualToString:INTEGOO_FETCH_FILTER]) {
        NSArray *newFilterArray = [self putIntoArrayFromJson:localFile intoContext:self.managedObjectContext withType:FILTERS andThenExecuteBlock:nil];
        [self.managedObjectContext performBlock:^{
            [Filter deleteOldObjectsFromNSArray:[self.oldFilters mutableCopy] compareNewArray:newFilterArray inManagedObjectContext:self.managedObjectContext withUser:self.user];
            [self.managedObjectContext save:NULL];
        }];
    } else if ([downloadTask.taskDescription isEqualToString:INTEGOO_FETCH_ICON]) {
        NSArray *newIconArray = [self putIntoArrayFromJson:localFile intoContext:self.managedObjectContext withType:ICONS andThenExecuteBlock:nil];
        [self.managedObjectContext performBlock:^{
            [Icon deleteOldObjectsFromNSArray:[self.oldIcons mutableCopy] compareNewArray:newIconArray inManagedObjectContext:self.managedObjectContext withUser:self.user];
            [self.managedObjectContext save:NULL];
        }];
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    // we don't support resuming an interrupted download task
}

// required by the protocol
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    // we don't report the progress of a download in our UI, but this is a cool method to do that with
}

- (void)application:(UIApplication *) application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"%@ didReceiveRemoteNotification %@ and context %@", self.nameClass, userInfo, self.managedObjectContext);
    if ([userInfo objectForKey:@"acme"]) {
        NSNumber *alertId = (NSNumber *) [userInfo objectForKey:@"acme"];
        //call AlertsCDTVC
        if ([Support getUser] != nil) {
            NSLog(@"%@ Integoo id %@ user is logged", self.nameClass, (NSString *)[userInfo objectForKey:@"acme"]);
            [self.managedObjectContext performBlock:^{
                User *user = [User fetchUser:self.managedObjectContext withUserName:[Support getUsername]];
                Alert *alert = [Alert findAlertFromRemotePushNotification:self.managedObjectContext withAlertId:alertId withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:user];
                if (alert != nil) {
                    NSDictionary *userInfo = alertId ? @{AlertId : alertId, AlertObject : alert, DatabaseAvailabityContext : self.managedObjectContext} : nil;
                    [[NSNotificationCenter defaultCenter] postNotificationName:MenuRemotePushNotification object:self userInfo:userInfo];
                } else {
                    NSLog(@"%@ alertId %@", self.nameClass, alertId);
                    NSDictionary *userInfo = alertId ? @{AlertId : alertId, DatabaseAvailabityContext : self.managedObjectContext} : nil;
                    [[NSNotificationCenter defaultCenter] postNotificationName:MenuRemotePushNotification object:self userInfo:userInfo];
                }
                [self.managedObjectContext save:NULL];
            }];
        } else {
            NSLog(@"%@ Integoo id isn't logged: %@", self.nameClass, (NSString *)[userInfo objectForKey:@"acme"]);
        }
    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"%@ Error didFailToRegisterForRemoteNotificationsWithError :%@", self.nameClass, str);
}

- (void)application:(UIApplication *) application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *tokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    [Support saveDeviceToken:tokenString];
    NSLog(@"%@ %@", self.nameClass, [NSString stringWithFormat:@"Device Token=%@",tokenString]);
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    if ([Support getTimeLogoutSwitcher] == YES) {
        NSLog(@"%@ applicationDidEnterBackground automaticLogout", self.nameClass);
        [Support saveLastTimeUserInApplication];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"%@ applicationWillEnterForeground automaticLogout", self.nameClass);
    if ([Support getTimeLogoutSwitcher] == YES && ([Support getUser] != nil)) {
        NSLog(@"%@ getTimeLogoutSwitcher YES", self.nameClass);
        NSTimeInterval nowInterval = [[NSDate date] timeIntervalSince1970];
        NSTimeInterval backgroundTime = [Support getLastTimeUserInApplication];
        double timePickerBackground = [Support getTimeAutomaticLogout];
        backgroundTime = backgroundTime + timePickerBackground;
        if (nowInterval > backgroundTime) {
            NSLog(@"%@ logout user time expire", self.nameClass);
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            SigninViewController *myViewController = [storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            myViewController.managedObjectContext = self.managedObjectContext;
            myViewController.logoutUser = TRUE;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:myViewController];
            self.window.rootViewController = navigationController;
            NSLog(@"%@ set visibleViewController %@", self.nameClass, ((UINavigationController*)self.window.rootViewController).visibleViewController);
        }
    }
}

@end
