//
//  DetailExistAlertViewController.h
//  thesis
//
//  Created by horkavlna on 17/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailExistAlertViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
