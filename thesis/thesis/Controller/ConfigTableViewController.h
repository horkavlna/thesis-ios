//
//  ConfigTableViewController.h
//  thesis
//
//  Created by horkavlna on 18/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfigTableViewController : UITableViewController <UITabBarDelegate, UITableViewDataSource, UIAlertViewDelegate, UIActionSheetDelegate, UIPickerViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
