//
//  AlertsCDTVC.m
//  thesis
//
//  Created by horkavlna on 25/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "NewAlertsCDTVC.h"
#import "DatabaseAvailabity.h"
#import "Alert.h"
#import "Support.h"
#import "DetailNewAlertViewController.h"
#import "Alert+Integoo.h"
#import "User+Integoo.h"
#import "Reachability.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "IntegooAlertTableViewCell.h"

#define ACTION_BAR_TABS_NEW_ALERT NSLocalizedStringFromTable(@"ACTION_BAR_TABS_NEW_ALERT", @"NewAlertsCDTVC", @"settings navigation controller")
#define NEW_ALERT_NO_LIST NSLocalizedStringFromTable(@"NEW_ALERT_NO_LIST", @"NewAlertsCDTVC", @"alert for new exist")
#define ALERT_NEXT_FETCH_FIRST NSLocalizedStringFromTable(@"ALERT_NEXT_FETCH_FIRST", @"NewAlertsCDTVC", @"alert next")
#define ALERT_NEXT_FETCH_SECOND NSLocalizedStringFromTable(@"ALERT_NEXT_FETCH_SECOND", @"NewAlertsCDTVC", @"alert")

#define ALERT_MESSAGE_NO_INTERNET_CONNECTION NSLocalizedStringFromTable(@"ALERT_MESSAGE_NO_INTERNET_CONNECTION", @"NetworkConnection", @"no internet connection")
#define ALERT_MESSAGE_BAD_NETWORK_REQUEST NSLocalizedStringFromTable(@"ALERT_MESSAGE_BAD_NETWORK_REQUEST", @"NetworkConnection", @"bad network connection")

@interface NewAlertsCDTVC ()

@property (strong, nonatomic) UILabel *invalidInputData;
@property (strong, nonatomic) NSTimer *elapsedTimeTimer;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) Alert *selectedAlert;
@property (strong, nonatomic) User *user;
@property (nonatomic, strong) NSNumber *alertId;
@property (strong, nonatomic) NSString *nameClass;

@end

@implementation NewAlertsCDTVC

//view controller getting notification from ThessisDelegate it is maximim chance before LifeCycle when you code put the awakeFromNib
- (void) awakeFromNib
{
    self.nameClass = [[NewAlertsCDTVC class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:AlertNewDatabaseAvailabityNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", self.nameClass, self.managedObjectContext);
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:AlertRemotePushNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.alertId = note.userInfo[AlertId];
        self.selectedAlert = note.userInfo[AlertObject];
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib AlertRemotePushNotification context %@", self.nameClass, self.managedObjectContext);
        if (self.alertId != nil) {
            NSLog(@"ALERTID %@",self.alertId);
            NSLog(@"ALERT exist %@",self.selectedAlert);
            if (self.selectedAlert == nil) {
                [self callRefresh:FALSE];
               // [self turnOffNetworkIndicatorAndResfreshControl:@"Alert doesn't exist" withTimeCreated:0];
            } else {
                NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext,AlertObject : self.selectedAlert} : nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:DetailNewAlertNotification object:self userInfo:userInfo];
                [self performSegueWithIdentifier:@"Show New Alert" sender:self];
            }
        }
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@ viewDidLoad", self.nameClass);
    self.navigationController.navigationBar.alpha = 1.0f;
    self.navigationController.navigationBar.translucent = NO;
    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:ACTION_BAR_TABS_NEW_ALERT];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%@ viewWillAppear", self.nameClass);
    if ([self.fetchedResultsController.fetchedObjects count] > 0) {
        if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
            [self setTimer];
        }
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    NSLog(@"%@ viewDidDisappear", self.nameClass);
    [self stopTimer];
}

- (void)dealloc {
    NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:AlertNewDatabaseAvailabityNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:AlertRemotePushNotification];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==0)
    {
        if (section == 0)
        {
            return ACTION_BAR_TABS_NEW_ALERT;
        }
    }
    return @"";
}

- (UIActivityIndicatorView *) activityIndicatorView
{
    if (!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    }
    return _activityIndicatorView;
}

- (void) setViewLabeNetworkError
{
    [self.invalidInputData setHidden:YES];
    self.invalidInputData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 25)];
    self.invalidInputData.textAlignment = NSTextAlignmentCenter;
    [self.tableView addSubview:self.invalidInputData];
}

- (void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    self.user = [User fetchUser:_managedObjectContext withUserName:[Support getUsername]];

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Alert"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnAlert == %@) AND (type == %@))", self.user, [NSNumber numberWithInt:NEW_ALERTS]];
    
    NSSortDescriptor * createdDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
    NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];

    [request setSortDescriptors:[NSArray arrayWithObjects:createdDescriptor, nameDescriptor, nil]];

    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
    NSLog(@"%@ fetchedResultsController count new alerts: %lu", self.nameClass, (unsigned long)[self.fetchedResultsController.fetchedObjects count]);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fetchedResultsController.fetchedObjects count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (([indexPath row] == [self.fetchedResultsController.fetchedObjects count]) && ([self.fetchedResultsController.fetchedObjects count])) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Next Alerts"];
        [cell.textLabel setText: [NSString stringWithFormat:@"%@ %d %@",
                                  ALERT_NEXT_FETCH_FIRST, [[Support getLimitFetch] integerValue], ALERT_NEXT_FETCH_SECOND]];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        self.activityIndicatorView.frame = CGRectMake(SCREEN_WIDTH/2 - 90.0f, 12.0f, 25.0f, 25.0f);
        [self.activityIndicatorView sizeToFit];
        [self.activityIndicatorView setColor:[UIColor grayColor]];
        [cell.contentView addSubview:self.activityIndicatorView];
    } else if ([self.fetchedResultsController.fetchedObjects count]) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell = [[IntegooAlertTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"New Alert Cell"];
        Alert *alert = [self.fetchedResultsController objectAtIndexPath:indexPath];
        cell.accessoryType=  UITableViewCellAccessoryDisclosureIndicator;
        
        UILabel *labelCell = [[UILabel alloc]init];
        [labelCell setTag:11101];
        labelCell.frame = CGRectMake(SCREEN_WIDTH - 130.0f,10.0f,100.0f,25.0f) ;
        [labelCell setBackgroundColor:[UIColor clearColor]];
        [labelCell setFont:[UIFont systemFontOfSize: 12]];
        [labelCell setTextColor:[UIColor blackColor]];
        
        NSDate *dateCreated = alert.created;
        NSTimeInterval seconds = ([[NSDate date] timeIntervalSince1970] - [dateCreated timeIntervalSince1970]);

        cell.textLabel.text = alert.title;
        cell.detailTextLabel.text = alert.place;
        cell.backgroundColor = [Support colorWithHexString:alert.color withAlpha:0.3f];
        labelCell.text = [Support durationTime:seconds];
        labelCell.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:labelCell];
        if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
            [self setTimer];
        }
    } else {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"New Alert Cell"];
    }
    return cell;
}

- (void) setTimer
{
    NSLog(@"%@ setTimer", self.nameClass);
    self.elapsedTimeTimer = [NSTimer scheduledTimerWithTimeInterval:1.000 target:self selector:@selector(reloadTableView) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.elapsedTimeTimer forMode:NSRunLoopCommonModes];
    [self.elapsedTimeTimer fire];
}

- (void) stopTimer
{
    NSLog(@"%@ stopTimer", self.nameClass);
    [self.elapsedTimeTimer invalidate];
    self.elapsedTimeTimer = nil;
}

- (void) reloadTableView
{
    [self.tableView reloadData];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == [self.fetchedResultsController.fetchedObjects count]) {
        [self.activityIndicatorView startAnimating];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        if (self.managedObjectContext) {
            NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
            if (base64Encoded != nil) {
                Alert *alert = [self.fetchedResultsController.fetchedObjects lastObject];
                NSLog(@"last object from %@ and created %f",alert.title,[alert.created timeIntervalSince1970]);
                NSLog(@"%@ last object for %@ and created %f", self.nameClass, alert.title, [alert.created timeIntervalSince1970]);
                [self checkNetworkConnection:base64Encoded lastCreated:[alert.created timeIntervalSince1970] * 1000];
            }
        }
    } else {
        self.selectedAlert = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSLog(@"%@ Alert %@",self.nameClass, self.selectedAlert.title);
        [self performSegueWithIdentifier:@"Show New Alert" sender:self];
    }
}

- (void) checkNetworkConnection:(NSString *)base64Encoded lastCreated: (NSTimeInterval) created
{
    dispatch_queue_t queue = dispatch_queue_create("network", NULL);
    dispatch_async(queue, ^{
        Reachability *rHostName = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus networkStatus = [rHostName currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_NO_INTERNET_CONNECTION withTimeCreated:created];
            });
        } else {
            NSMutableArray *arrayAllAlerts = [[NSMutableArray alloc] init];
            [self.managedObjectContext performBlock:^{
                [arrayAllAlerts addObjectsFromArray:[Alert fetchAllAlerts:self.managedObjectContext
                                                                 withType:[NSNumber numberWithInt:NEW_ALERTS] withUser: self.user]];
                [self.managedObjectContext save:NULL];
            }];
            [self startSesstionDownloadNewAlerts:base64Encoded lastCreated:created withOffset:0
                    withOldAlerts:arrayAllAlerts wihNewArray:[[NSMutableArray alloc] init]];
        }
    });
}

- (IBAction)refreshControl:(UIRefreshControl *)sender {
    [self callRefresh:TRUE];
}

- (void) callRefresh:(BOOL) callRefreshControl
{
    [self setViewLabeNetworkError];
    [self.refreshControl beginRefreshing];
    if (!callRefreshControl) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y-self.refreshControl.frame.size.height) animated:YES];
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    if (self.managedObjectContext) {
        NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
        if (base64Encoded != nil) {
            [self checkNetworkConnection:base64Encoded lastCreated:0];
        } else {
            NSLog(@"%@ base64Encoded is nil", self.nameClass);
        }
    } else {
        NSLog(@"%@ hasn't context", self.nameClass);
    }
}

- (void) checkAlertForRemoteNotification: (NSTimeInterval) created
{
    NSLog(@"%@ checkAlertForRemoteNotification alertId %@", self.nameClass, self.alertId);
    NSMutableString *error = [[NSMutableString alloc] init];
    if (self.alertId != nil) {
        __block Alert *alert = nil;
        [self.managedObjectContext performBlock:^{
           alert = [Alert findAlertFromRemotePushNotification:self.managedObjectContext
                                                  withAlertId:self.alertId withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
            [self.managedObjectContext save:NULL];
        }];
        if (alert == nil) {
            //error
            NSLog(@"%@ checkAlertForRemoteNotification alert doesn't exist", self.nameClass);
            [error appendString:NEW_ALERT_NO_LIST];
        } else {
            NSLog(@"%@ checkAlertForRemoteNotification call detail alert", self.nameClass);
            self.selectedAlert = alert;
            [self performSegueWithIdentifier:@"Show New Alert" sender:self];
        }
        self.alertId = nil;
    }
    [self turnOffNetworkIndicatorAndResfreshControl:error withTimeCreated:created];
}

- (void) startSesstionDownloadNewAlerts:(NSString *) base64Encoded lastCreated: (NSTimeInterval)created
                             withOffset:(NSUInteger)offset withOldAlerts: (NSArray*)oldAlerts wihNewArray: (NSMutableArray *) newArray
{
    void(^doSomething)(void) = ^(void) {
        NSLog(@"%@ startSesstionDownloadNewAlerts created: %f and limit %@", self.nameClass, created, [Support getLimitFetch]);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_NEW_ALERTS] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
        [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
        [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
        [request addValue:[NSString stringWithFormat: @"%f", created] forHTTPHeaderField:HEADER_X_CREATED];
        [request addValue:[NSString stringWithFormat: @"%@", [Support getLimitFetch]] forHTTPHeaderField:HEADER_X_LIMIT];
        [request addValue:[NSString stringWithFormat: @"%d", (int)offset] forHTTPHeaderField:HEADER_X_OFFSET];
        [request setHTTPMethod:HTTP_METHOD_GET];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        configuration.timeoutIntervalForRequest = TIME_INTERVAL_FOREGRAUND_REQUEST;
        configuration.allowsCellularAccess = NO;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
            completionHandler:^(NSURL *url, NSURLResponse *response, NSError *error) {
                    if (!error) {
                        if ([request.URL isEqual:[Support getWholeAddress:URL_NEW_ALERTS]]) {
                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                            if ([httpResponse statusCode] == 200) {
                                    NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                                    NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                                    NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                                    if (created == 0) {     //created == 0 fetch all database
                                        NSArray *newPartArray = [self putIntoCoreData:url intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                                        [newArray addObjectsFromArray: newPartArray];
                                        NSLog(@"%@ whole alerts count %lu",self.nameClass, (unsigned long)newArray.count);
                                        if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
                                            NSLog(@"%@ offset:%d", self.nameClass, [[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] +
                                                  [[Support getLimitFetch] integerValue]);
                                            [self.refreshControl endRefreshing];
                                            [self startSesstionDownloadNewAlerts:base64Encoded lastCreated:created withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue] withOldAlerts:oldAlerts wihNewArray:newArray];
                                            
                                        } else {
                                            [self stopTimer];
                                            [self.managedObjectContext performBlock:^{
                                                [Alert deleteOldObjectsFromNSArray:[oldAlerts mutableCopy]  compareNewArray:newArray inManagedObjectContext:self.managedObjectContext withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
                                                [self.managedObjectContext save:NULL];
                                            }];
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
                                                    [self setTimer];
                                                }
                                                [self checkAlertForRemoteNotification:created];
                                            });
                                        }
                                    } else {    //creted > 0 fetch next limit
                                        [self putIntoCoreData:url intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self turnOffNetworkIndicatorAndResfreshControl:nil withTimeCreated:created];
                                        });
                                    }
                                
                                } else {
                                    NSLog(@"%@ ERROR - Authentication response status code: %ld", self.nameClass, (long)[httpResponse statusCode]);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withTimeCreated:created];
                                    });
                                }
                            } else {
                                NSLog(@"%@ ERROR - bad request", self.nameClass);
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withTimeCreated:created];
                                });
                            }
                        } else {
                            NSLog(@"%@ ERROR - bad request", self.nameClass);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withTimeCreated:created];
                            });
                        }
                    }];
        [downloadTask resume];
    };
    doSomething();
}

- (void) turnOffNetworkIndicatorAndResfreshControl:(NSString *)errorMessage withTimeCreated: (NSTimeInterval) created
{
    if(created > 0) {
        [self.activityIndicatorView stopAnimating];
    } else {
        [self.refreshControl endRefreshing];
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if((errorMessage != nil) && (errorMessage.length > 0)) {
        [self setViewLabeNetworkError];
        [Support wrongAuthentication:errorMessage withLable:self.invalidInputData];
    }
}

- (NSMutableArray *)integooAlertsAtURL:(NSURL *)data
{
    NSMutableArray *alert;
    NSData *integooJSONData = [NSData dataWithContentsOfURL:data];  // will block if url is not local!
    if (integooJSONData) {
        NSError *error;
        alert = [NSJSONSerialization JSONObjectWithData:integooJSONData
                                                options:0
                                                  error:&error];
    }
    return alert;
}

- (NSArray *) putIntoCoreData:(NSURL *)url intoContext:(NSManagedObjectContext *)context andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *alert = [[self integooAlertsAtURL:url] mutableCopy];
        [context performBlock:^{
            [Alert loadAlertsFromIntegooArray:alert intoManagedObjectContext:context withType:[NSNumber numberWithInt:NEW_ALERTS] withUser:self.user];
            [context save:NULL];
            if (whenDone) whenDone();
        }];
        return alert;
    } else {
        if (whenDone) whenDone();
    }
    return nil;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show New Alert"]) {
        if ([segue.destinationViewController isKindOfClass:[DetailNewAlertViewController class]]) {
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext,AlertObject : self.selectedAlert} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:DetailNewAlertNotification object:self userInfo:userInfo];
            self.alertId = nil;
            self.selectedAlert = nil;
        }
    }
}

@end
