//
//  DetailAlertViewController.h
//  thesis
//
//  Created by horkavlna on 25/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Alert.h"

@interface DetailNewAlertViewController : UIViewController <UIAlertViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
