//
//  FilterTableViewController.m
//  thesis
//
//  Created by horkavlna on 18/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "FilterTableViewController.h"
#import "DatabaseAvailabity.h"
#import "Support.h"
#import "FilterCDTVC.h"
#import "Filter+Integoo.h"
#import "IntegooAlertTableViewCell.h"
#import "User+Integoo.h"

#define FILTER_TITLE NSLocalizedStringFromTable(@"FILTER_TITLE", @"FilterTableViewController", @"title for navigation controller")
#define FILTER_NO_TITLE NSLocalizedStringFromTable(@"FILTER_NO_TITLE", @"FilterTableViewController", @"title no exist filter")

@interface FilterTableViewController ()

@property (strong, nonatomic) FilterCDTVC *filterCDTVC;
@property (strong, nonatomic) NSString *nameClass;
@property (strong, nonatomic) User *user;

@end

@implementation FilterTableViewController

- (void) awakeFromNib
{
    self.nameClass = [[FilterTableViewController class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:FilterOneNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", [FilterTableViewController class], self.managedObjectContext);
    }];
}

- (void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    self.user = [User fetchUser:_managedObjectContext withUserName:[Support getUsername]];
    NSLog(@"%@ fetchedResultsController count filters alerts: %lu", self.nameClass, (unsigned long)[self.fetchedResultsController.fetchedObjects count]);
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Filter"];
    request.predicate = [NSPredicate predicateWithFormat:@"((whoOwnFilter == %@) AND (isChecked == 1))", self.user];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                              ascending:YES]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"%@ viewWillAppear", self.nameClass);
    [self.tableView reloadData];
}

- (void)dealloc {
	NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:FilterOneNotification];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	NSLog(@"%@ viewDidLoad", self.nameClass);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectedFilterCell"];
    cell.textLabel.text = FILTER_TITLE;
    NSString *filterName;
    if ([self.fetchedResultsController.fetchedObjects count] > 0) {
        Filter *filter = [self.fetchedResultsController objectAtIndexPath:indexPath];
        filterName = filter.name;
        NSLog(@"%@ filterName %@", self.nameClass, filterName);
    } else {
        filterName = FILTER_NO_TITLE;
    }
    UILabel *labelCell = [[UILabel alloc]init];
    labelCell.frame = CGRectMake(SCREEN_WIDTH - 150.0f,10.0f,120.0f,25.0f) ;
    [labelCell setBackgroundColor:[UIColor clearColor]];
    
    labelCell.text = filterName;
    labelCell.textAlignment = NSTextAlignmentRight;
    [cell.contentView addSubview:labelCell];    
    return cell;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.tableView reloadData];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Show New Filter" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show New Filter"]) {
        if ([segue.destinationViewController isKindOfClass:[FilterCDTVC class]]) {
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:FilterSearchNotification object:self userInfo:userInfo];
        }
    }
}
@end
