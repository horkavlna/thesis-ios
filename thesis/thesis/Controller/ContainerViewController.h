//
//  ContainerViewController.h
//  thesis
//
//  Created by horkavlna on 18/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewController : UIViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
