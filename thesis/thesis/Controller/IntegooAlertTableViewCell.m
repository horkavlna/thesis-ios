//
//  IntegooAlertTableViewCell.m
//  integoo
//
//  Created by horkavlna on 03/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "IntegooAlertTableViewCell.h"

@implementation IntegooAlertTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(5,5,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
    self.detailTextLabel.frame = CGRectMake(5,25,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, self.bounds.size.width, self.bounds.size.height)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.layer.borderWidth = 0.25;
    [self.contentView addSubview:separatorView];
}

@end
