//
//  ExistAlertsCDTVC.h
//  thesis
//
//  Created by horkavlna on 11/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface ExistAlertsCDTVC : CoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
