//
//  SigninViewController.m
//  thesis
//
//  Created by horkavlna on 13/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "SigninViewController.h"
#import "Support.h"
#import "MenuViewController.h"
#import "Reachability.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "DatabaseAvailabity.h"
#import "EventsCDTVC.h"
#import "User+Integoo.h"

#define TITLE_NAVIGATION_CONTROLLER NSLocalizedStringFromTable(@"ACTION_BAR_SIGNIN", @"SigninViewController", @"title for navigation controller")
#define RIGHT_BUTTON_NAVIGATION_CONTROLLER NSLocalizedStringFromTable(@"ACTION_BAR_DONE", @"SigninViewController", @"Button on the right side")
#define UITEXTFIELD_USERNAME NSLocalizedStringFromTable(@"PLACEHOLDER_USERNAME", @"SigninViewController", @"textfield for username")
#define UITEXTFIELD_PASSWORD NSLocalizedStringFromTable(@"PLACEHOLDER_PASSWORD", @"SigninViewController", @"textfield for password")
#define ERROR_WRONG_USERNAME_PASSWORD NSLocalizedStringFromTable(@"ALERT_MESSAGE_USERNAME_PASSWORD", @"SigninViewController", @"wrong username and password")
#define ALERT_MESSAGE_USERNAME NSLocalizedStringFromTable(@"ALERT_MESSAGE_USERNAME", @"SigninViewController", @"Alert message bad username")
#define ALERT_MESSAGE_PASSWORD NSLocalizedStringFromTable(@"ALERT_MESSAGE_PASSWORD", @"SigninViewController", @"Alert message bad password")

#define ALERT_MESSAGE_NO_INTERNET_CONNECTION NSLocalizedStringFromTable(@"ALERT_MESSAGE_NO_INTERNET_CONNECTION", @"NetworkConnection", @"no internet connection")
#define ALERT_MESSAGE_BAD_NETWORK_REQUEST NSLocalizedStringFromTable(@"ALERT_MESSAGE_BAD_NETWORK_REQUEST", @"NetworkConnection", @"bad network connection")

@interface  SigninViewController() <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *usernameImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UINavigationItem *signin;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel *invalidInputData;
@property (strong, nonatomic) NSString *nameClass;

@end

@implementation SigninViewController

- (void) awakeFromNib
{
    self.nameClass = [[SigninViewController class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:SigninDatabaseAvailabityNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", self.nameClass, self.managedObjectContext);
    }];
}

- (void)dealloc {
    NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:SigninDatabaseAvailabityNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@ viewDidLoad", self.nameClass);

}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"%@ viewWillAppear", self.nameClass);
    [[NSNotificationCenter defaultCenter] addObserverForName:WrongNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self stopNetworkAndActivityIndicator];
    }];
    NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
    if (base64Encoded != nil && self.logoutUser != TRUE) {
        [self performSegueWithIdentifier:@"signin" sender:self];
    } else {
        [self setNavigationBar];
        [self.invalidInputData setHidden:YES];
        
        UIImage *usernameImage = [UIImage imageNamed:@"username"];
        self.usernameImageView.image = usernameImage;
        
        UIImage *passwordImage = [UIImage imageNamed:@"password"];
        self.passwordImageView.image = passwordImage;
        
        [self setPlaceHolder];
        [self setKeyboardTextField];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:WrongNotification];
}

- (void) setNavigationBar
{
    self.navigationController.navigationBar.alpha = 1.0f;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    [self.navigationItem setHidesBackButton:YES];
    self.signin.title = TITLE_NAVIGATION_CONTROLLER;
    [self setTitleRightBarButton];
}

- (void) setPlaceHolder
{
    NSString *defaultUser = [Support getDefaultUser];
    if (defaultUser == nil) {
        self.userTextField.placeholder = UITEXTFIELD_USERNAME;
    } else {
        self.userTextField.text = defaultUser;
    }
    self.passwordTextField.placeholder = UITEXTFIELD_PASSWORD;
}

- (UIActivityIndicatorView *) activityIndicatorView
{
    if (!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    }
    return _activityIndicatorView;
}

- (void) setKeyboardTextField
{
    self.userTextField.returnKeyType = UIReturnKeyNext;
    self.userTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.passwordTextField.returnKeyType = UIReturnKeyDone;
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (self.activityIndicatorView.isAnimating) {
        NSLog(@"ActivyIndicator is animate can't connect");
        return NO;
    } else {
        if (textField == self.userTextField) {
            [self.passwordTextField becomeFirstResponder];
        } else if (textField == self.passwordTextField) {
            [self.passwordTextField endEditing:YES];
            if ([self checkLengthUsernameAndPassword] == 0) {
                [self checkNetworkConnection];
            }
        } else {
            [self resignFirstResponder];
            return NO;
        }
    }
    return YES;
}

- (void) checkNetworkConnection
{
    NSLog(@"%@ checkNetworkConnection", self.nameClass);
    [self.passwordTextField resignFirstResponder];
    dispatch_queue_t queue = dispatch_queue_create("network", NULL);
    dispatch_async(queue, ^{
        Reachability *rHostName = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus networkStatus = [rHostName currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Support wrongAuthentication:ALERT_MESSAGE_NO_INTERNET_CONNECTION withLable:self.invalidInputData];
            });
        } else {
            [self startSessionAuthentication:[Support getUsernameAndPasswordForBase64Encoded:self.userTextField.text withPassword:self.passwordTextField.text]];
        }
    });
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"signin"]) {
        if ([segue.destinationViewController isKindOfClass:[MenuViewController class]]) {
            NSLog(@"%@ context %@",self.nameClass, self.managedObjectContext);
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:EventDatabaseAvailabityNotification object:self userInfo:userInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:AlertNewDatabaseAvailabityNotification object:self userInfo:userInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:AlertExistDatabaseAvailabityNotification object:self userInfo:userInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:MenuDatabaseAvailabityNotification object:self userInfo:userInfo];
//            [[NSNotificationCenter defaultCenter] postNotificationName:ConfigDatabaseAvailabityNotification object:self userInfo:userInfo];
//            [[NSNotificationCenter defaultCenter] postNotificationName:DetailEventNotification object:self userInfo:userInfo];
//            [[NSNotificationCenter defaultCenter] postNotificationName:DetailNewAlertNotification object:self userInfo:userInfo];
//            [[NSNotificationCenter defaultCenter] postNotificationName:DetailExistAlertNotification object:self userInfo:userInfo];
        }
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"signin"]) {
        if ([self checkLengthUsernameAndPassword] == 0) {
            [self checkNetworkConnection];
        }
    }
    return FALSE;
}

- (void) startSessionAuthentication: (NSString *) usernameAndPasswordFormatEncodedBase64
{
    NSLog(@"%@ startSessionAuthentication %@", self.nameClass, [Support getWholeAddress:URL_AUTHENTICATION]);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_AUTHENTICATION] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
    [request addValue:usernameAndPasswordFormatEncodedBase64 forHTTPHeaderField:HEADER_AUTHORIZATION];
    [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_CONTENT_TYPE];
    [request setHTTPMethod:HTTP_METHOD_POST];
    NSDictionary *imeiAuthentication = @{@"imei":[Support getDeviceToken]};
    NSData *body = [NSJSONSerialization dataWithJSONObject:imeiAuthentication options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:body];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.timeoutIntervalForRequest = TIME_INTERVAL_FOREGRAUND_REQUEST;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error) {
                    if ([request.URL isEqual:[Support getWholeAddress:URL_AUTHENTICATION]]) {
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        if ([httpResponse statusCode] == 201) {
                            NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                            NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                            NSLog(@"%@ OK - Authentication response header:%@", self.nameClass, [httpResponse allHeaderFields]);
                            if (self.logoutUser == TRUE) {
                                self.logoutUser = FALSE;
                                if ([[Support getUsername] isEqualToString:self.userTextField.text]) {
                                    NSLog(@"%@ user was only log out", self.nameClass);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self performSegueWithIdentifier:@"signin" sender:self];
                                        [self turnOffNetworkIndicator:nil];
                                    });
                                } else {
                                    NSLog(@"%@ different user want login and previous user will logout %@", self.nameClass, [Support getUsername]);
                                    if ([Support getUsername] != nil) {
                                        [self startSessionLogout:[[httpResponse allHeaderFields] objectForKey:HEADER_LOCATION]];
                                    } else {
                                        NSLog(@"%@ error %@", self.nameClass, [Support getUsername]);
                                        [NSException raise:@"User no exist" format:@"class %@", self.nameClass];
                                    }
                                }
                            } else {
                                [Support removeUser];
                                [Support saveUser:self.userTextField.text withPassword:self.passwordTextField.text];
                                NSString *responseHeaderLocation = [[httpResponse allHeaderFields] objectForKey:HEADER_LOCATION];
                                [self putIntoCoreData:responseHeaderLocation intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self performSegueWithIdentifier:@"signin" sender:self];
                                    [self turnOffNetworkIndicator:nil];
                                });
                            }
                        } else {
                            NSLog(@"%@ ERROR - Authentication response status code: %d", self.nameClass, [httpResponse statusCode]);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicator:ERROR_WRONG_USERNAME_PASSWORD];
                            });
                        }
                    } else {
                        NSLog(@"%@ ERROR - bad request", self.nameClass);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                        });
                    }

                } else {
                    NSLog(@"%@ ERROR - bad request", self.nameClass);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                    });
                }
            }];
    [uploadTask resume];
}

- (void) startSessionLogout: (NSString *)responseHeader
{
    NSLog(@"startSessionLogout username:%@",[Support getUsername]);
    User *user = [User fetchUser:self.managedObjectContext withUserName:[Support getUsername]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:user.idLocation] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
    
    [request addValue:[Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil] forHTTPHeaderField:HEADER_AUTHORIZATION];
    [request setHTTPMethod:HTTP_METHOD_DELETE];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.timeoutIntervalForRequest = 30;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request
          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
              if (!error) {
                  if ([request.URL isEqual:[Support getWholeAddress:user.idLocation]]) {
                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                      if ([httpResponse statusCode] == 204) {
                          NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                          NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                          NSLog(@"%@ OK - Authentication response:%@", self.nameClass, response);
                          [Support removeUser];
                          [Support saveUser:self.userTextField.text withPassword:self.passwordTextField.text];
                          NSString *responseHeaderLocation = responseHeader;
                          [self putIntoCoreData:responseHeaderLocation intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self performSegueWithIdentifier:@"signin" sender:self];
                              [self turnOffNetworkIndicator:nil];
                          });
                      } else {
                          NSLog(@"%@ ERROR - Authentication response status code: %d", self.nameClass, [httpResponse statusCode]);
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                          });
                      }
                  } else {
                      NSLog(@"%@ ERROR - bad request", self.nameClass);
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                      });
                  }
              } else {
                  NSLog(@"%@ ERROR - bad request", self.nameClass);
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                  });

              }
          }];
    [uploadTask resume];
}

- (void) turnOffNetworkIndicator:(NSString *)errorMessage
{
    if(errorMessage != nil) {
        [Support wrongAuthentication:errorMessage withLable:self.invalidInputData];
    } else {
        [self stopNetworkAndActivityIndicator];
    }
}

//return 0 - username and password >0, 1 - username = 0, 2 - password = 0
- (NSUInteger) checkLengthUsernameAndPassword
{
    [self showNetworkAndActivityIndicator];
    if ((self.userTextField.text.length > 0) && (self.passwordTextField.text.length > 0)) {
        return 0;
    } else if (self.userTextField.text.length == 0){
        [Support wrongAuthentication:ALERT_MESSAGE_USERNAME withLable:self.invalidInputData];
        return 1;
    } else {
        [Support wrongAuthentication:ALERT_MESSAGE_PASSWORD withLable:self.invalidInputData];
        return 2;
    }
}

- (NSDictionary *)integooUser:(NSString *)idLocation
{
    return @{USER_IDLOCATION: idLocation, USER_IMEI: [Support getDeviceToken], USER_USERNAME: [Support getUsername], USER_PASSWORD: [Support getPassword]};
}

- (void)putIntoCoreData:(NSString *)idLocation intoContext:(NSManagedObjectContext *)context andThenExecuteBlock:(void(^)())whenDone
{
    NSLog(@"%@ put new user to database", self.nameClass);
    if (context) {
        NSDictionary *user = [self integooUser:idLocation];
        [context performBlock:^{
            [User saveUser:user intoManagedObjectContext:context];
            [context save:NULL];
            if (whenDone) whenDone();
        }];
    } else {
        if (whenDone) whenDone();
    }
}

- (void) showNetworkAndActivityIndicator
{
    //network activitiy indicator
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.activityIndicatorView sizeToFit];
    [self.activityIndicatorView setColor:[UIColor grayColor]];
    [self.activityIndicatorView setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicatorView];
    [self.navigationItem setRightBarButtonItem:rightBarButton animated:YES];
    [self.activityIndicatorView startAnimating];
}

- (void) setTitleRightBarButton
{
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:RIGHT_BUTTON_NAVIGATION_CONTROLLER style:UIBarButtonItemStylePlain target:self action:@selector(clickAgainRightBarButton:)];
    [self.navigationItem setRightBarButtonItem:rightBarButton animated:YES];
}

- (void) stopNetworkAndActivityIndicator
{
    NSLog(@"stopNetworkAndActivityIndicator");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.activityIndicatorView stopAnimating];
    [self setTitleRightBarButton];
}

- (void) clickAgainRightBarButton: (UIBarButtonItem *)rightBarButton
{
    [self.passwordTextField endEditing:YES];
    [self shouldPerformSegueWithIdentifier:@"signin" sender:self];
}

@end
