//
//  EmptySegue.m
//  thesis
//
//  Created by horkavlna on 18/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "EmptySegue.h"

@implementation EmptySegue

- (void)perform
{
    // Nothing. The ContainerViewController class handles all of the view
    // controller action.
}
@end
