//
//  IntegooEventTableViewCell.m
//  integoo
//
//  Created by horkavlna on 03/06/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "IntegooEventTableViewCell.h"

@implementation IntegooEventTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(10,5,30,30);
    self.textLabel.frame = CGRectMake(50,5,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
    self.detailTextLabel.frame = CGRectMake(50,25,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, self.bounds.size.width, self.bounds.size.height)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.layer.borderWidth = 0.25;
    [self.contentView addSubview:separatorView];
}

@end
