//
//  SigninViewController.h
//  thesis
//
//  Created by horkavlna on 15/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SigninViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) BOOL logoutUser;
@end
