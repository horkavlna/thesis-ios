//
//  EventsViewController.h
//  thesis
//
//  Created by horkavlna on 13/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"

@interface EventsCDTVC : CoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
