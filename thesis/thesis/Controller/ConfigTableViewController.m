//
//  ConfigTableViewController.m
//  thesis
//
//  Created by horkavlna on 18/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "ConfigTableViewController.h"
#import "MenuViewController.h"
#import "SigninViewController.h"
#import "Support.h"
#import "DatabaseAvailabity.h"
#import "User+Integoo.h"
#import "Reachability.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

#define PREFERENCE_SETTINGS_TITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_TITLE", @"ConfigTableViewController", @"settings navigation controller")
#define PREFERENCE_SETTINGS_NOTIFICATION NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_NOTIFICATION", @"ConfigTableViewController", @"group tableView group for remote push notification")
#define PREFERENCE_SETTINGS_SERVER NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_SERVER", @"ConfigTableViewController", @"settings navigation controller")
#define PREFERENCE_SETTINGS_USER NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_USER", @"ConfigTableViewController", @"settings navigation controller")
#define PREFERENCE_SETTINGS_PUSH_NOTIFICATION NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_PUSH_NOTIFICATION", @"ConfigTableViewController", @"subtitle name for alarm push notification")
#define PREFERENCE_SETTINGS_AUTOMATIC_FETCH_TITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_AUTOMATIC_FETCH_TITLE", @"ConfigTableViewController", @"subtitle for limit in one fetch")
#define PREFERENCE_SETTINGS_DEFAULT_USER NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_DEFAULT_USER", @"ConfigTableViewController", @"subtitle default user")
#define PREFERENCE_SETTINGS_TIME_AUTOMATIC_LOGOUT_SUBTITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_TIME_AUTOMATIC_LOGOUT_SUBTITLE", @"ConfigTableViewController", @"subtitle automatic logout")
#define PREFERENCE_SETTINGS_TIME_AUTOMATIC_LOGOUT_TITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_TIME_AUTOMATIC_LOGOUT_TITLE", @"ConfigTableViewController", @"subtitle automatic logout")
#define PREFERENCE_SETTINGS_LOGOUT NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_LOGOUT", @"ConfigTableViewController", @"subtitle  logout")
#define PREFERENCE_SETTINGS_OK NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_OK", @"ConfigTableViewController", @"alert button ok")
#define PREFERENCE_SETTINGS_SET NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_SET", @"ConfigTableViewController", @"alert button set")
#define PREFERENCE_SETTINGS_CANCEL NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_CANCEL", @"ConfigTableViewController", @"alert button cancel")
#define PREFERENCE_SETTINGS_SERVER_ADDRESS NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_SERVER_ADDRESS", @"ConfigTableViewController", @"alert server address")
#define PREFERENCE_SETTINGS_LOGOUT_TITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_LOGOUT_TITLE", @"ConfigTableViewController", @"alert question if user is suer")
#define PREFERENCE_SETTINGS_FETCH_LIMIT_TITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_FETCH_LIMIT_TITLE", @"ConfigTableViewController", @"size limit one fetch")
#define PREFERENCE_SETTINGS_AUTOMATIC_FETCH_LIMIT_SUBTITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_AUTOMATIC_FETCH_LIMIT_SUBTITLE", @"ConfigTableViewController", @"size limit one fetch")
#define PREFERENCE_SETTINGS_SERVER_ADDRESS_SUBTITLE NSLocalizedStringFromTable(@"PREFERENCE_SETTINGS_SERVER_ADDRESS_SUBTITLE", @"ConfigTableViewController", @"change server address")

#define ALERT_MESSAGE_NO_INTERNET_CONNECTION NSLocalizedStringFromTable(@"ALERT_MESSAGE_NO_INTERNET_CONNECTION", @"NetworkConnection", @"no internet connection")
#define ALERT_MESSAGE_BAD_NETWORK_REQUEST NSLocalizedStringFromTable(@"ALERT_MESSAGE_BAD_NETWORK_REQUEST", @"NetworkConnection", @"bad network connection")

//tag for section x (x - NSINTEGER), tag for UIVIEW in sextion xyz (y,z - NSINTEGER)
@interface ConfigTableViewController ()

@property (strong, nonatomic) UILabel *invalidInputData;
@property (nonatomic) NSInteger selectedValueLimitFetcher;
@property (strong, nonatomic) NSString *nameClass;
@property (strong, nonatomic) NSString *temporalyServerAddress;

@end

@implementation ConfigTableViewController

NSString *const LOGINCELL = @"loginCell";
NSInteger TAGALERTSERVERADDRESS = 1;
NSInteger TAGALERTDEAFULTUSER = 2;
NSInteger TAGALERTDEAFULTFETCHLIMIT = 3;
NSInteger TAGALERTDEAFULTTIME = 4;
NSInteger TAGALERTLOGOUT = 5;
NSInteger TAGALERTLOGOUTCHANGESERVERADDRESS = 6;

//view controller getting notification from ThessisDelegate it is maximim chance before LifeCycle when you code put the awakeFromNib
- (void) awakeFromNib
{
    self.nameClass = [[ConfigTableViewController class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:ConfigDatabaseAvailabityNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", [ConfigTableViewController class], self.managedObjectContext);
    }];
}

- (void)dealloc {
    NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:ConfigDatabaseAvailabityNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@ viewDidLoad", self.nameClass);
    self.navigationItem.title = PREFERENCE_SETTINGS_TITLE;
	self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self leftBarButtonView]];
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"%@ viewWillAppear", self.nameClass);
}

- (void) setViewLabeNetworkError
{
    [self.invalidInputData setHidden:YES];
    self.invalidInputData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 25)];
    self.invalidInputData.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.invalidInputData];
}

- (UIView *)leftBarButtonView
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"left"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickLeftBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 30, 30)];
    return button;
}

- (void) clickLeftBackButton: (UIButton*)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == 0) {
        if (section == 0) {
            return PREFERENCE_SETTINGS_NOTIFICATION;
        }
        if (section == 1) {
            return PREFERENCE_SETTINGS_SERVER;
        }
        if (section == 2) {
            return PREFERENCE_SETTINGS_USER;
        }
    }
    return @"";
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch ([indexPath section]) {
        case 0:
            if ([indexPath row] == 0) {
                cell = [self setPushNotificationCellTableView:tableView withIdentifier:LOGINCELL andTextLabel:PREFERENCE_SETTINGS_PUSH_NOTIFICATION];
            }
            break;
        case 1:
            if ([indexPath row] == 0) {
                cell = [self setStandardCellTableView:tableView withIdentifier:LOGINCELL andTextLabel:[Support getServerAddress]];
            }
            if ([indexPath row] == 1) {
                cell = [self setStandardCellTableView:tableView withIdentifier:LOGINCELL andTextLabel:[NSString stringWithFormat:@"%@: %@",PREFERENCE_SETTINGS_FETCH_LIMIT_TITLE, [Support getLimitFetch]]];
            }
            break;
        case 2:
            if ([indexPath row] == 0) {
                NSMutableString *defaultUser = [[NSMutableString alloc] initWithString:PREFERENCE_SETTINGS_DEFAULT_USER];
                if ([Support getDefaultUser]!= nil) {
                    [defaultUser appendString:@":"];
                    [defaultUser appendString:[Support getDefaultUser]];
                }
                cell = [self setStandardCellTableView:tableView withIdentifier:LOGINCELL andTextLabel:[NSString stringWithString:defaultUser]];
            } else if ([indexPath row] == 1) {
                cell = [self setAutomaticTimerLogoutForCellTableView:tableView withIdentifier:LOGINCELL andTextLabel:PREFERENCE_SETTINGS_TIME_AUTOMATIC_LOGOUT_TITLE];
            } else if ([indexPath row] == 2){
                cell = [self setLogOutCellTableView:tableView withIdentifier:LOGINCELL andTextLabel:PREFERENCE_SETTINGS_LOGOUT];
            }
            break;
    }
    return cell;
}

- (UITableViewCell *) setPushNotificationCellTableView:(UITableView *)tableView withIdentifier:(NSString *) identifier andTextLabel:(NSString *) text
{
    UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
    if ([[Support getRegisterForRemoteNotification] intValue] == 2) {
        switchView.on = NO;
    } else {
        switchView.on = YES;
    }
    [switchView addTarget:self action:@selector(handlerSwitcherRemotePushNotification:) forControlEvents:UIControlEventValueChanged];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    [[cell contentView] setBackgroundColor:[UIColor clearColor]];
    [[cell backgroundView] setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = text;
    cell.accessoryView = switchView;
    return cell;
}


- (UITableViewCell *) setStandardCellTableView:(UITableView *)tableView withIdentifier:(NSString *) identifier andTextLabel:(NSString *) text
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    [[cell contentView] setBackgroundColor:[UIColor clearColor]];
    [[cell backgroundView] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.text = text;
    [cell.textLabel setTextColor:[Support standardColorForSettingsCellTableView]];
    return cell;
}

- (UITableViewCell *) setAutomaticTimerLogoutForCellTableView:(UITableView *)tableView withIdentifier:(NSString *) identifier andTextLabel:(NSString *) text
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
    switchView.on = [Support getTimeLogoutSwitcher];
    [switchView addTarget:self action:@selector(handlerSwitcherTimeLogout:) forControlEvents:UIControlEventValueChanged];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    [[cell contentView] setBackgroundColor:[UIColor clearColor]];
    [[cell backgroundView] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:20];
    cell.textLabel.text = [self showTimmer];
    [cell.textLabel setTextColor:[Support standardColorForSettingsCellTableView]];
    cell.detailTextLabel.text = text;
    cell.accessoryView = switchView;
    return cell;
}

- (UITableViewCell *) setLogOutCellTableView:(UITableView *)tableView withIdentifier:(NSString *) identifier andTextLabel:(NSString *) text
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    [[cell contentView] setBackgroundColor:[UIColor clearColor]];
    [[cell backgroundView] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.text = text;
    [cell.textLabel setTextColor:[Support standardColorForSettingsCellTableView]];
    return cell;
}


- (void) handlerSwitcherRemotePushNotification:(id) sender
{
    NSLog(@"handlerSwitcherRemotePushNotification %hhd",[sender isOn]);
    if ([sender isOn]) {
        [Support registerForRemoteNotification];
    } else {
        [Support unregisterForRemoteNotification];
    }
}

- (void) handlerSwitcherTimeLogout:(id)sender
{
    NSLog(@"handlerSwitcherTimeLogout %hhd",[sender isOn]);
    [Support saveTimeLogoutSwitcher:![sender isOn]];
}

- (NSString *)showTimmer
{
    NSMutableString *timeSeparateHourAndMinute = [[NSMutableString alloc] init];
    int timeInSeconds = [Support getTimeAutomaticLogout];
    //HOURS
    if((timeInSeconds/60) >= 60) {
        [timeSeparateHourAndMinute appendString:[NSString stringWithFormat:@"%d:",(timeInSeconds/3600)]];
        timeInSeconds = timeInSeconds / 60;
    } else {
        [timeSeparateHourAndMinute appendString:@"0:"];
    }
    if ((timeInSeconds/60) < 60) {
        [timeSeparateHourAndMinute appendString:[NSString stringWithFormat:@"%.2d",(timeInSeconds/60)]];
    } else {
        [timeSeparateHourAndMinute appendString:@"00"];
    }
    return timeSeparateHourAndMinute;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"%@ indexPath section: %d", self.nameClass, indexPath.section);
    switch ([indexPath section]) {
        case 0:
            break;
        case 1:
            if ([indexPath row] == 0) {
                [self setDefault:PREFERENCE_SETTINGS_SERVER_ADDRESS cancelButton:PREFERENCE_SETTINGS_CANCEL otherButton:PREFERENCE_SETTINGS_SET withIdentifier:TAGALERTSERVERADDRESS title:[Support getServerAddress]];
            }
            if ([indexPath row] == 1) {
                [self setCountDownFetchLimit];
            }
            break;
        case 2:
            if ([indexPath row] == 0) {
                [self setDefault:PREFERENCE_SETTINGS_DEFAULT_USER cancelButton:PREFERENCE_SETTINGS_CANCEL otherButton:PREFERENCE_SETTINGS_SET withIdentifier:TAGALERTDEAFULTUSER title:[Support getDefaultUser]];
            } else if ([indexPath row] == 1) {
                [self setDataPickerTimeLogout];
            } else if ([indexPath row] == 2){
                [Support showAlertTitle:nil withMessage:PREFERENCE_SETTINGS_LOGOUT_TITLE withDelegate:self cancelButton:PREFERENCE_SETTINGS_CANCEL otherButton:PREFERENCE_SETTINGS_LOGOUT withIdentifier:TAGALERTLOGOUT];
            }
            break;
        default:
            break;
    }
}

- (void) setDefault:(NSString *)title cancelButton: (NSString *) cancelButtonTitle otherButton:(NSString *) otherButtonTitle withIdentifier: (NSInteger) tagId title: (NSString *) textTextField
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:otherButtonTitle, nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *defaultTextField = [alert textFieldAtIndex:0];
    defaultTextField.text = textTextField;
    alert.tag = tagId;
    [alert show];
}

- (void) setCountDownFetchLimit
{
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:[NSString stringWithFormat:@"%@%@", title, PREFERENCE_SETTINGS_AUTOMATIC_FETCH_LIMIT_SUBTITLE]
                                  delegate:self cancelButtonTitle:PREFERENCE_SETTINGS_CANCEL destructiveButtonTitle:nil otherButtonTitles:PREFERENCE_SETTINGS_SET, nil];
    [actionSheet showInView:self.view];
    UIPickerView *fetchLimit = [[UIPickerView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2) - 26, 10, 50, 100)];
    fetchLimit.delegate = self;
    fetchLimit.showsSelectionIndicator = YES;
    NSLog(@"index%lu",(unsigned long)[[Support getFetchLimitItem] indexOfObject:[NSString stringWithFormat:@"%@",[Support getLimitFetch]]]);
    [fetchLimit selectRow:[[Support getFetchLimitItem] indexOfObject:[NSString stringWithFormat:@"%@",[Support getLimitFetch]]] inComponent:0 animated:YES];
    [actionSheet addSubview:fetchLimit];
    actionSheet.tag = TAGALERTDEAFULTFETCHLIMIT;
}

- (void) setDataPickerTimeLogout
{
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:[NSString stringWithFormat:@"%@%@", title, PREFERENCE_SETTINGS_TIME_AUTOMATIC_LOGOUT_SUBTITLE]
                                  delegate:self cancelButtonTitle:PREFERENCE_SETTINGS_CANCEL destructiveButtonTitle:nil otherButtonTitles:PREFERENCE_SETTINGS_SET, nil];
    [actionSheet showInView:self.view];
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
    [datePicker setCountDownDuration:[Support getTimeAutomaticLogout]];
    [actionSheet addSubview:datePicker];
    actionSheet.tag = TAGALERTDEAFULTTIME;
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (popup.tag == TAGALERTDEAFULTTIME) {
            UIDatePicker *datePicker = [[popup subviews] lastObject];
            NSLog(@"%@ get countDown %f", self.nameClass, datePicker.countDownDuration);
            [Support saveTimeAutomaticLogout:datePicker.countDownDuration];
            [self.tableView reloadData];
        }
        else if (popup.tag == TAGALERTDEAFULTFETCHLIMIT) {
            NSNumber *valueLimitFetch = [[Support getFetchLimitItem] objectAtIndex:(NSUInteger)self.selectedValueLimitFetcher];
            NSLog(@"%@ defaultFetchLimit %@", self.nameClass, valueLimitFetch);
            [Support saveLimitFetch:valueLimitFetch];
            [self.tableView reloadData];
        }
    }
}


- (void) checkNetworkConnection
{
    dispatch_queue_t queue = dispatch_queue_create("network", NULL);
    dispatch_async(queue, ^{
        Reachability *rHostName = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus networkStatus = [rHostName currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.temporalyServerAddress != nil) {
                    [Support saveServerAddress:self.temporalyServerAddress];
                    self.temporalyServerAddress = nil;
                }
                [self.tableView reloadData];
                [self turnOffNetworkIndicator:ALERT_MESSAGE_NO_INTERNET_CONNECTION];
            });
        } else {
            [self startSessionLogout];
        }
    });
}

- (void) startSessionLogout
{
    NSLog(@"startSessionLogout username:%@",[Support getUsername]);
    User *user = [User fetchUser:self.managedObjectContext withUserName:[Support getUsername]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:user.idLocation] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
    
    [request addValue:[Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil] forHTTPHeaderField:HEADER_AUTHORIZATION];
    [request setHTTPMethod:HTTP_METHOD_DELETE];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.timeoutIntervalForRequest = 30;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error) {
                    if ([request.URL isEqual:[Support getWholeAddress:user.idLocation]]) {
                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                         if ([httpResponse statusCode] == 204) {
                             NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                             NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                             NSLog(@"%@ OK - Authentication response:%@", self.nameClass, response);
                             [Support removeUser];
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [self performSegueWithIdentifier:@"logout" sender:self];
                                 [self turnOffNetworkIndicator:nil];
                             });
                          } else {
                             NSLog(@"%@ ERROR - Authentication response status code: %d", self.nameClass, [httpResponse statusCode]);
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 if (self.temporalyServerAddress != nil) {
                                     [Support saveServerAddress:self.temporalyServerAddress];
                                     self.temporalyServerAddress = nil;
                                 }
                                 [self.tableView reloadData];
                                 [self turnOffNetworkIndicator:[NSString stringWithFormat:@"%@-%d",ALERT_MESSAGE_BAD_NETWORK_REQUEST, [httpResponse statusCode]]];
                             });
                          }
                    } else {
                        NSLog(@"%@ ERROR - bad request", self.nameClass);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.temporalyServerAddress != nil) {
                                [Support saveServerAddress:self.temporalyServerAddress];
                                self.temporalyServerAddress = nil;
                            }
                            [self.tableView reloadData];
                            [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                        });
                    }
                 } else {
                     NSLog(@"%@ ERROR - bad request", self.nameClass);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         if (self.temporalyServerAddress != nil) {
                             [Support saveServerAddress:self.temporalyServerAddress];
                             self.temporalyServerAddress = nil;
                         }
                         [self.tableView reloadData];
                         [self turnOffNetworkIndicator:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                     });
                 }
            }];
    [uploadTask resume];
}

- (void) turnOffNetworkIndicator:(NSString *)errorMessage
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if(errorMessage != nil) {
        [self setViewLabeNetworkError];
        [Support wrongAuthentication:errorMessage withLable:self.invalidInputData];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"logout"]) {
        if ([segue.destinationViewController isKindOfClass:[SigninViewController class]]) {
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:SigninDatabaseAvailabityNotification object:self userInfo:userInfo];
            [self annimation];
        }
    }
}

- (void) annimation
{
    [UIView beginAnimations:@"ShowDetails" context: nil];
    //[UIView setAnimationCurve: UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == TAGALERTSERVERADDRESS) {
        if (buttonIndex == 1) {
            NSString *serverAddress = [alertView textFieldAtIndex:0].text;
            if ([serverAddress compare:[Support getServerAddress]] != NSOrderedSame) {
                self.temporalyServerAddress = serverAddress;
                [Support showAlertTitle:PREFERENCE_SETTINGS_SERVER_ADDRESS_SUBTITLE withMessage:nil withDelegate:self cancelButton:PREFERENCE_SETTINGS_CANCEL otherButton:PREFERENCE_SETTINGS_OK withIdentifier:TAGALERTLOGOUTCHANGESERVERADDRESS];
            } else {
                NSLog(@"%@ ServerAddress are same no action %@", self.nameClass, serverAddress);
            }
        }
    } else if (alertView.tag == TAGALERTDEAFULTUSER) {
        if (buttonIndex == 1) {
            NSString *defaultUser = [alertView textFieldAtIndex:0].text;
            [Support saveDefaultUser:defaultUser];
            NSLog(@"%@ DefaultUser %@", self.nameClass, defaultUser);
            [self.tableView reloadData];
        }
    } else if (alertView.tag == TAGALERTLOGOUT) {
        if (buttonIndex != 0) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            [self checkNetworkConnection];
        }
    } else if (alertView.tag == TAGALERTLOGOUTCHANGESERVERADDRESS) {
        if (buttonIndex != 0) {
            NSString *previousValue = [Support getServerAddress];
            [Support saveServerAddress:self.temporalyServerAddress];
            self.temporalyServerAddress = previousValue;
            [self.tableView reloadData];
            NSLog(@"%@ ok change server address %@", self.nameClass, self.temporalyServerAddress);
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            [self checkNetworkConnection];
        }
    }
}

#pragma PickerView delegate and datasource

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedValueLimitFetcher = row;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[Support getFetchLimitItem] count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[Support getFetchLimitItem] objectAtIndex:row];
}

@end
