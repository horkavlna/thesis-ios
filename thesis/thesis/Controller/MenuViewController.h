//
//  MenuViewController.h
//  thesis
//
//  Created by horkavlna on 15/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UITabBarController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSNumber *alertId;

- (void) setTitleForNavigationItemBar;

@end
