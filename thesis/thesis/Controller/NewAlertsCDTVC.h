//
//  AlertsCDTVC.h
//  thesis
//
//  Created by horkavlna on 25/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface NewAlertsCDTVC : CoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
