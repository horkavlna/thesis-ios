//
//  DetailExistAlertViewController.m
//  thesis
//
//  Created by horkavlna on 17/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "DetailExistAlertViewController.h"
#import "DatabaseAvailabity.h"
#import "Support.h"
#import "Alert+Integoo.h"
#import "Icon+Integoo.h"
#import "MenuViewController.h"
#import "Reachability.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

#define TITLE_FOR_DOWNLOAD_IMAGE NSLocalizedStringFromTable(@"TITLE_FOR_DOWNLOAD_IMAGE", @"DetailExistAlertViewController", @"title for download for image")

#define ALERT_MESSAGE_NO_INTERNET_CONNECTION NSLocalizedStringFromTable(@"ALERT_MESSAGE_NO_INTERNET_CONNECTION", @"NetworkConnection", @"no internet connection")
#define ALERT_MESSAGE_BAD_NETWORK_REQUEST NSLocalizedStringFromTable(@"ALERT_MESSAGE_BAD_NETWORK_REQUEST", @"NetworkConnection", @"bad network connection")

@interface DetailExistAlertViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *deviceImage;
@property (weak, nonatomic) IBOutlet UILabel *device;
@property (weak, nonatomic) IBOutlet UILabel *destination;
@property (weak, nonatomic) IBOutlet UILabel *created;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UIImageView *imageDetail;
@property (weak, nonatomic) IBOutlet UIButton *downloadImage;
@property (weak, nonatomic) IBOutlet UILabel *downloadImageLabel;
@property (strong, nonatomic) UITextView *instructions;
@property (strong, nonatomic) UILabel *invalidInputData;
@property (nonatomic, strong) Alert *alert;
@property (strong, nonatomic) NSTimer *elapsedTimeTimer;
@property (nonatomic) BOOL animatingImage;
@property (strong, nonatomic) NSString *nameClass;

@end

@implementation DetailExistAlertViewController

- (void) awakeFromNib
{
    self.nameClass = [[DetailExistAlertViewController class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:DetailExistAlertNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        self.alert = note.userInfo[AlertObject];
        NSLog(@"%@ awakeFromNib %@ and new alert %@", self.nameClass, self.managedObjectContext, self.alert);
    }];
}

- (void)viewDidLoad
{
    NSLog(@"%@ viewDidLoad", self.nameClass);
    [super viewDidLoad];
    self.scrollView.delegate = self;
    self.navigationItem.title = self.alert.title;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self leftBarButtonView]];
    self.device.text = self.alert.device;
    self.destination.text = self.alert.place;
    self.created.text = [self timeAndDateCreated:self.alert.created];
    self.duration.text = [Support durationTime:[self durationAlert]];
    [self setButtonLoadImage];
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    [self willRotateToInterfaceOrientation:interfaceOrientation duration:0.0f];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%@ viewWillAppear", self.nameClass);
    [[NSNotificationCenter defaultCenter] addObserverForName:WrongNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self stopNetworkAndActivityIndicator];
    }];
    if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
        [self setTimer];
    }
    if (self.imageDetail.image == nil) {
        [self.imageDetail setBackgroundColor:[UIColor lightGrayColor]];
    } else {
        [self.imageDetail setBackgroundColor:[UIColor clearColor]];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    NSLog(@"%@ viewDidDisappear", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:WrongNotification];
    [self stopTimer];
}

-(void)dealloc {
    NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:DetailExistAlertNotification];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.instructions removeFromSuperview];
    NSLog(@"%@ willRotateToInterfaceOrientation", self.nameClass);
    [self setInstructionValue: toInterfaceOrientation];
}

- (void) setInstructionValue: (UIInterfaceOrientation) toInterfaceOrientation
{
    CGRect screenSize = [Support getScreenFrameForOrientation:toInterfaceOrientation];
    CGRect frameInstructions = CGRectMake(self.deviceImage.frame.origin.x, self.downloadImage.frame.origin.y + self.downloadImage.frame.size.height + 80, screenSize.size.width - (self.deviceImage.frame.origin.x + self.deviceImage.frame.origin.x), 0);
    self.instructions = [[UITextView alloc] initWithFrame:frameInstructions];
    [self.instructions setText:[NSString stringWithCString:[[self.alert.instructions stringByReplacingOccurrencesOfString:@"//" withString:@"\\u0002"] cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding]];
    CGSize size = [Support sizeOfText:self.instructions.text widthOfTextView:self.instructions.frame.size.width withFont:self.instructions.font];
    frameInstructions.size.height = size.height + 40;
    [self.instructions setFrame:frameInstructions];
    [self.instructions setScrollEnabled:NO];
    self.instructions.editable = NO;
    self.instructions.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
    NSLog(@"%@ scrollView width %f", self.nameClass, frameInstructions.origin.y + frameInstructions.size.height - 20);
    [self.scrollView addSubview:self.instructions];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    NSLog(@"%@ viewDidLayoutSubviews", self.nameClass);
    self.scrollView.contentSize = CGSizeMake(self.instructions.frame.origin.x, self.instructions.frame.origin.y + self.instructions.frame.size.height - 30);
}

- (void) setButtonLoadImage
{
    if (self.alert.img != nil) {
        self.imageDetail.image = [[UIImage alloc] initWithData:self.alert.img];
        [self.downloadImage setHidden:YES];
        [self.downloadImageLabel setHidden:YES];
    } else {
        [self.downloadImage setHidden:NO];
        [self.downloadImageLabel setText:TITLE_FOR_DOWNLOAD_IMAGE];
    }
}

- (void) setTimer
{
    NSLog(@"%@ setTimer", self.nameClass);
    self.elapsedTimeTimer = [NSTimer scheduledTimerWithTimeInterval:1.000
                                                             target:self selector:@selector(reloadDuration) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.elapsedTimeTimer forMode:NSRunLoopCommonModes];
    [self.elapsedTimeTimer fire];
}

- (void) stopTimer
{
    NSLog(@"%@ stopTimer", self.nameClass);
    [self.elapsedTimeTimer invalidate];
    self.elapsedTimeTimer = nil;
}

- (void) reloadDuration
{
    self.duration.text = [Support durationTime:[self durationAlert]];
    [self.duration setNeedsDisplay];
}

- (NSTimeInterval) durationAlert
{
    NSDate *dateCreated = self.alert.created;
    return ([[NSDate date] timeIntervalSince1970] - [dateCreated timeIntervalSince1970]);
}

- (NSString *) timeAndDateCreated: (NSDate *)created
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy--MM--dd HH:mm"];
    return [dateFormatter stringFromDate:created];
}

- (void) setViewLabeNetworkError
{
    [self.invalidInputData setHidden:YES];
    self.invalidInputData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 25)];
    self.invalidInputData.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.invalidInputData];
}

- (UIView *)leftBarButtonView
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"left"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickLeftBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 30, 30)];
    return button;
}

- (void) clickLeftBackButton: (UIButton*)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)downloadImage:(id)sender {
    [self setDisabledImageButton];
    self.animatingImage = YES;
    [self spinButtonImage:UIViewAnimationOptionCurveEaseIn];
    [self checkContextAndUser:[Support getWholeAddress:[NSString stringWithFormat:@"%@%@%@",URL_ALERTS_IMAGE,self.alert.idAlert.stringValue,URL_IMAGE]]
               withHttpMethod:HTTP_METHOD_GET andType:EXIST_ALERTS];
}

- (void) spinButtonImage: (UIViewAnimationOptions) options
{
    [UIView animateWithDuration:0.5f delay:0.0f options:options animations:^{
        self.downloadImage.transform = CGAffineTransformRotate(self.downloadImage.transform, M_PI / 2);
    } completion:^(BOOL completed){
        if (completed) {
            if (self.animatingImage) {
                [self spinButtonImage: UIViewAnimationOptionCurveLinear];
            }
        }
    }];
}

- (void) checkNetworkConnection:(NSString *)base64Encoded withUrl: (NSURL *) url withHttpMethod: (NSString *) httpMethod andType:(NSInteger)type
{
    dispatch_queue_t queue = dispatch_queue_create("network", NULL);
    dispatch_async(queue, ^{
        Reachability *rHostName = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus networkStatus = [rHostName currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_NO_INTERNET_CONNECTION withType:type];
            });
        } else {
            [self startSesstionDownloadNewImage:base64Encoded withUrl:url withHttpMethod:httpMethod andType:type];
        }
    });
}

- (void) turnOffNetworkIndicatorAndResfreshControl:(NSString *)errorMessage withType: (NSInteger)type
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if (type == EXIST_ALERTS) {
        self.animatingImage = NO;
        [self setButtonLoadImage];
    } else if (type == NEW_ALERTS) {
        
    }
    if(errorMessage != nil) {
        [self setViewLabeNetworkError];
        [Support wrongAuthentication:errorMessage withLable:self.invalidInputData];
    } else {
        [self setEnabledImageButton];
    }
}

- (void) startSesstionDownloadNewImage:(NSString *) base64Encoded withUrl: (NSURL *)url withHttpMethod: (NSString *)httpMethod andType:(NSInteger) type
{
    NSLog(@"%@ startSesstionDownloadNewImage", self.nameClass);
    if (self.managedObjectContext) {
        NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
        if (base64Encoded != nil) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
            [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
            [request setHTTPMethod:httpMethod];
            
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
            configuration.timeoutIntervalForRequest = TIME_INTERVAL_FOREGRAUND_REQUEST;
            configuration.allowsCellularAccess = NO;
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
            
            NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if (!error) {
                        if ([request.URL isEqual:[Support getWholeAddress:[NSString stringWithFormat:@"%@%@%@",URL_ALERTS_IMAGE,self.alert.idAlert.stringValue,URL_IMAGE]]]) {
                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                            if ([httpResponse statusCode] == 200) {
                                NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                                NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                                [self putIntoCoreData:request.URL intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.imageDetail setBackgroundColor:[UIColor clearColor]];
                                    [self turnOffNetworkIndicatorAndResfreshControl:nil withType:type];
                                    [self setEnabledImageButton];
                                });
                            } else {
                                NSLog(@"%@ ERROR - bad request", self.nameClass);
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withType:type];
                                });
                            }
                        } else {
                            NSLog(@"%@ ERROR - bad request", self.nameClass);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withType:type];
                            });
                        }
                    } else {
                        NSLog(@"%@ ERROR - bad request", self.nameClass);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withType:type];
                        });
                    }
                }];
            [downloadTask resume];
        }
    }
}

- (NSMutableArray *)integooImageAlertsAtURL:(NSURL *)data
{
    NSMutableArray *alert;
    NSData *integooJSONData = [NSData dataWithContentsOfURL:data];  // will block if url is not local!
    if (integooJSONData) {
        NSError *error;
        alert = [NSJSONSerialization JSONObjectWithData:integooJSONData
                                                options:0
                                                  error:&error];
    }
    return alert;
}

- (NSArray *) putIntoCoreData:(NSURL *)url intoContext:(NSManagedObjectContext *)context andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *alertImage = [[self integooImageAlertsAtURL:url] mutableCopy];
        [context performBlock:^{
            [Alert saveImage:self.alert addImage:alertImage intoManagedObjectContext:(NSManagedObjectContext *)context];
            [context save:NULL];
            if (whenDone) whenDone();
        }];
        return alertImage;
    } else {
        if (whenDone) whenDone();
    }
    return nil;
}

- (void) checkContextAndUser: (NSURL *) url withHttpMethod: (NSString *) httpMethod andType:(NSInteger)type
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    if (self.managedObjectContext) {
        NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
        if (base64Encoded != nil) {
            [self checkNetworkConnection:base64Encoded withUrl:url withHttpMethod:httpMethod andType:type];
        } else {
            [self setEnabledImageButton];
        }
    } else {
        [self setEnabledImageButton];
    }
}

- (void) setEnabledImageButton
{
    [self.downloadImage setEnabled:YES];
}

- (void) setDisabledImageButton
{
    [self.downloadImage setEnabled:NO];
}

- (void) stopNetworkAndActivityIndicator
{
    NSLog(@"%@ stopNetworkAndActivityIndicator", self.nameClass);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self setEnabledImageButton];
}

@end
