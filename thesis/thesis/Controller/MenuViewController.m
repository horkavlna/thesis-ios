//
//  MenuViewController.m
//  thesis
//
//  Created by horkavlna on 15/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "MenuViewController.h"
#import "Support.h"
#import "ConfigTableViewController.h"
#import "DatabaseAvailabity.h"

@interface MenuViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItemBar;

@end

@implementation MenuViewController

- (void) awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserverForName:MenuDatabaseAvailabityNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", [MenuViewController class], self.managedObjectContext);
    }];
}

- (void)dealloc {
    NSLog(@"dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:MenuDatabaseAvailabityNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:MenuRemotePushNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserverForName:MenuRemotePushNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.selectedIndex = 0;
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        if (note.userInfo[AlertObject] == nil) {
            NSDictionary *userInfo = note.userInfo[AlertId] ? @{AlertId : note.userInfo[AlertId], DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:AlertRemotePushNotification object:self userInfo:userInfo];
        } else {
            NSDictionary *userInfo = note.userInfo[AlertId] ? @{AlertId : note.userInfo[AlertId], AlertObject : note.userInfo[AlertObject], DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:AlertRemotePushNotification object:self userInfo:userInfo];
        }
    }];
    [self.navigationItem setHidesBackButton:YES];
    [self setTitleForNavigationItemBar];
    [self.rightBarButton setCustomView:[self rightBarButtonView]];
    self.navigationController.navigationBar.alpha = 1.0f;
    self.navigationController.navigationBar.translucent = NO;
}

- (void) setTitleForNavigationItemBar
{
    self.navigationItemBar.title = [Support getUsername];
}

- (UIView *)rightBarButtonView
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"settings" ] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickConfigBarButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 50)];
    return button;
}

- (void)clickConfigBarButton: (UIButton*)sender
{
    NSLog(@"clickConfigBarButton");
    [self performSegueWithIdentifier:@"settings" sender:self];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"settings"]) {
        if ([segue.destinationViewController isKindOfClass:[ConfigTableViewController class]]) {
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:ConfigDatabaseAvailabityNotification object:self userInfo:userInfo];
        }
    }
}

@end
