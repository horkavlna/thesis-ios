//
//  FilterCDTVC.m
//  thesis
//
//  Created by horkavlna on 18/03/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "FilterCDTVC.h"
#import "EventsCDTVC.h"
#import "DatabaseAvailabity.h"
#import "IntegooFilterTableViewCell.h"
#import "Filter.h"
#import "Support.h"
#import "Reachability.h"
#import "User+Integoo.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "Filter+Integoo.h"
#import "Icon+Integoo.h"

#define FILTER_TABLE_VIEW_TITLE NSLocalizedStringFromTable(@"FILTER_TABLE_VIEW_TITLE", @"FilterCDTVC", @"filters table view title")
#define FILTER_NO_LIST NSLocalizedStringFromTable(@"FILTER_NO_LIST", @"FilterCDTVC", @"title for navigation controller")

#define ALERT_MESSAGE_NO_INTERNET_CONNECTION NSLocalizedStringFromTable(@"ALERT_MESSAGE_NO_INTERNET_CONNECTION", @"NetworkConnection", @"no internet connection")
#define ALERT_MESSAGE_BAD_NETWORK_REQUEST NSLocalizedStringFromTable(@"ALERT_MESSAGE_BAD_NETWORK_REQUEST", @"NetworkConnection", @"bad network connection")

@interface FilterCDTVC ()

@property (strong, nonatomic) UILabel *invalidInputData;
@property (strong, nonatomic) Filter *filter;
@property (strong, nonatomic) Filter *selectedFilter;
@property (strong, nonatomic) NSString *nameClass;
@property (strong, nonatomic) User *user;

@end

@implementation FilterCDTVC

- (void) awakeFromNib
{
    self.nameClass = [[FilterCDTVC class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:FilterSearchNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", self.nameClass, self.managedObjectContext);
    }];
}

- (void)dealloc {
    NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:FilterSearchNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = FILTER_TABLE_VIEW_TITLE;
	self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self leftBarButtonView]];
    self.selectedFilter = [Filter fetchSelectedFilterManagedObjectContext:self.managedObjectContext withUser:self.user];
}

- (void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    self.user = [User fetchUser:_managedObjectContext withUserName:[Support getUsername]];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Filter"];
    request.predicate = [NSPredicate predicateWithFormat:@"whoOwnFilter == %@", self.user];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                              ascending:YES]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
    NSLog(@"%@ fetchedResultsController count filters alerts: %lu", self.nameClass, (unsigned long)[self.fetchedResultsController.fetchedObjects count]);
}

- (void) setViewLabeNetworkError
{
    [self.invalidInputData setHidden:YES];
    self.invalidInputData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 25)];
    self.invalidInputData.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.invalidInputData];
}

- (UIView *)leftBarButtonView
{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"left"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickLeftBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 30, 30)];
    return button;
}

- (void) clickLeftBackButton: (UIButton*)sender {
    if (self.filter != nil) {
        [self.managedObjectContext performBlock:^{
            self.filter.isChecked = [NSNumber numberWithInt:1];
            self.selectedFilter.isChecked = [NSNumber numberWithInt:0];
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:FilterOneNotification object:self userInfo:userInfo];
            
            NSDictionary *filterInfo = @{FilterObject : self.filter};
            [[NSNotificationCenter defaultCenter] postNotificationName:FilterSelected object:self userInfo:filterInfo];
        }];
    }
	[self.navigationController popViewControllerAnimated:YES];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==0)
    {
        if (section == 0)
        {
            return FILTER_TABLE_VIEW_TITLE;
        }
    }
    return @"";
}

- (void) turnOffNetworkIndicatorAndResfreshControl:(NSString *)errorMessage
{
    [self.refreshControl endRefreshing];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if(errorMessage != nil) {
        [self setViewLabeNetworkError];
        [Support wrongAuthentication:errorMessage withLable:self.invalidInputData];
    }
}

- (void) checkNetworkConnection:(NSString *)base64Encoded
{
    dispatch_queue_t queue = dispatch_queue_create("network", NULL);
    dispatch_async(queue, ^{
        Reachability *rHostName = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus networkStatus = [rHostName currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_NO_INTERNET_CONNECTION];
            });
        } else {
            [self startSesstionDownloadNewFilters:base64Encoded withOldFilters:[Filter fetchAllFilters:self.managedObjectContext withUser:self.user] withUrl: URL_FILTER];
        }
    });
}

- (IBAction)refreshControlFilterSearch:(id)sender {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    if (self.managedObjectContext) {
        NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
        if (base64Encoded != nil) {
            [self checkNetworkConnection:base64Encoded];
        }
    }
}


- (void) startSesstionDownloadNewFilters:(NSString *) base64Encoded withOldFilters: (NSArray*)oldFilters withUrl: (NSString *) url
{
    void(^doSomething)(void) = ^(void) {
        NSLog(@"%@ startSesstionDownloadNewAlerts", self.nameClass);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
        [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
        [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
        [request setHTTPMethod:HTTP_METHOD_GET];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        configuration.timeoutIntervalForRequest = TIME_INTERVAL_FOREGRAUND_REQUEST;
        configuration.allowsCellularAccess = NO;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
            completionHandler:^(NSURL *url, NSURLResponse *response, NSError *error) {
                if (!error) {
                    if ([request.URL isEqual:[Support getWholeAddress:URL_FILTER]]) {
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        if ([httpResponse statusCode] == 200) {
                            NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                            NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                            NSArray *newFilterArray = [self putIntoFilterCoreData:url intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                            NSLog(@"%@ newFilterArray %lu", self.nameClass, (unsigned long)newFilterArray.count);
                            [self.managedObjectContext performBlock:^{
                                [Filter deleteOldObjectsFromNSArray:[oldFilters mutableCopy] compareNewArray:newFilterArray inManagedObjectContext:self.managedObjectContext withUser:self.user];
                                    [self.managedObjectContext save:NULL];
                            }];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:nil];
                                [self startSesstionDownloadNewFilters:base64Encoded withOldFilters:[Icon fetchAllIcon:self.managedObjectContext withUser:self.user]withUrl:URL_ICON];
                            });
                        } else {
                            NSLog(@"%@ ERROR - Authentication response status code: %ld", self.nameClass, (long)[httpResponse statusCode]);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                            });
                        }
                    } else if ([request.URL isEqual:[Support getWholeAddress:URL_ICON]]) {
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        if ([httpResponse statusCode] == 200) {
                            NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                            NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                            NSArray *newIconArray = [self putIntoIconCoreData:url intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                            NSLog(@"%@ newIconArray %lu", self.nameClass, (unsigned long)newIconArray.count);
                            [self.managedObjectContext performBlock:^{
                                NSLog(@"%@ deleteOldObjectsFromNSArray", self.nameClass);
                                [Icon deleteOldObjectsFromNSArray:[oldFilters mutableCopy] compareNewArray:newIconArray inManagedObjectContext:self.managedObjectContext withUser:self.user];
                                    [self.managedObjectContext save:NULL];
                            }];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:nil];
                            });
                        } else {
                            NSLog(@"%@ ERROR - Authentication response status code: %ld", self.nameClass, (long)[httpResponse statusCode]);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                            });
                        }
                    } else {
                        NSLog(@"%@ ERROR - bad request", self.nameClass);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                        });
                    }
                } else {
                    NSLog(@"%@ ERROR - bad request", self.nameClass);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST];
                    });
                }
            }];
        [downloadTask resume];
    };
    doSomething();
}


- (NSMutableArray *)integooFilterAtURL:(NSURL *)data
{
    NSMutableArray *alert;
    NSData *integooJSONData = [NSData dataWithContentsOfURL:data];  // will block if url is not local!
    if (integooJSONData) {
        NSError *error;
        alert = [NSJSONSerialization JSONObjectWithData:integooJSONData
                                                options:0
                                                  error:&error];
    }
    return alert;
}

- (NSMutableArray *)integooIconAtURL:(NSURL *)data
{
    NSMutableArray *alert;
    NSData *integooJSONData = [NSData dataWithContentsOfURL:data];  // will block if url is not local!
    if (integooJSONData) {
        NSError *error;
        alert = [NSJSONSerialization JSONObjectWithData:integooJSONData
                                                options:0
                                                  error:&error];
    }
    return alert;
}

- (NSArray *) putIntoFilterCoreData:(NSURL *)url intoContext:(NSManagedObjectContext *)context andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *filter = [[self integooFilterAtURL:url] mutableCopy];
        [context performBlock:^{
            [Filter loadFiltersFromIntegooArray:filter intoManagedObjectContext:context withUser:self.user];
            [context save:NULL];
            if (whenDone) whenDone();
        }];
        return filter;
    } else {
        if (whenDone) whenDone();
    }
    return nil;
}

- (NSArray *) putIntoIconCoreData:(NSURL *)url intoContext:(NSManagedObjectContext *)context andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *icon = [[self integooIconAtURL:url] mutableCopy];
        [context performBlock:^{
             [Icon loadIconsFromIntegooArray:icon intoManagedObjectContext:context withUser:self.user];
            [context save:NULL];
            if (whenDone) whenDone();
        }];
        return icon;
    } else {
        if (whenDone) whenDone();
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITableViewCell *cell = [[IntegooFilterTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Filter Search Cell"];
    Filter *filter = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = filter.name;
    if (0 == [filter.isChecked intValue]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.filter = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSLog(@"didSelectRowAtIndexPath: %@",self.filter.idFilter);
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self iterateAllCell:tableView];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self clickLeftBackButton:nil];
}

- (void) iterateAllCell: (UITableView *)tableView
{
    for (int row = 0; row < [tableView numberOfRowsInSection:0]; row++) {
        NSIndexPath* cellPath = [NSIndexPath indexPathForRow:row inSection:0];
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:cellPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
