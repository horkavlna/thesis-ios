//
//  EventsViewController.m
//  thesis
//
//  Created by horkavlna on 13/02/14.
//  Copyright (c) 2014 Filip Reznicek. All rights reserved.
//

#import "EventsCDTVC.h"
#import "Event.h"
#import "DatabaseAvailabity.h"
#import "Support.h"
#import "Event+Integoo.h"
#import "DetailEventViewController.h"
#import "ContainerViewController.h"
#import "User+Integoo.h"
#import "Filter+Integoo.h"
#import "Icon+Integoo.h"
#import "IntegooEventTableViewCell.h"
#import "Reachability.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

#define EVENT_NEXT_FETCH_SECOND NSLocalizedStringFromTable(@"EVENT_NEXT_FETCH_SECOND", @"EventsCDTVC", @"title bar")
#define FILTER_NO_TITLE NSLocalizedStringFromTable(@"FILTER_NO_TITLE", @"EventsCDTVC", @"error if the filter is't set")
#define NEXT_EVENTS NSLocalizedStringFromTable(@"NEXT_EVENTS", @"EventsCDTVC", @"title for button next x limit evetns")

#define ALERT_MESSAGE_NO_INTERNET_CONNECTION NSLocalizedStringFromTable(@"ALERT_MESSAGE_NO_INTERNET_CONNECTION", @"NetworkConnection", @"no internet connection")
#define ALERT_MESSAGE_BAD_NETWORK_REQUEST NSLocalizedStringFromTable(@"ALERT_MESSAGE_BAD_NETWORK_REQUEST", @"NetworkConnection", @"bad network connection")

@interface EventsCDTVC ()

@property (nonatomic, weak) ContainerViewController *containerViewController;
@property (strong, nonatomic) UILabel *invalidInputData;
@property (strong, nonatomic) NSTimer *elapsedTimeTimer;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) Event *selectedEvent;
@property (strong, nonatomic) NSString *nameClass;
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) Filter *filter;

@end

@implementation EventsCDTVC

- (void) awakeFromNib
{
    self.nameClass = [[EventsCDTVC class] description];
    [[NSNotificationCenter defaultCenter] addObserverForName:EventDatabaseAvailabityNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.managedObjectContext = note.userInfo[DatabaseAvailabityContext];
        NSLog(@"%@ awakeFromNib %@", self.nameClass, self.managedObjectContext);
    }];
}

- (void)dealloc {
    NSLog(@"%@ dealloc", self.nameClass);
    [[NSNotificationCenter defaultCenter] removeObserver:EventDatabaseAvailabityNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:FilterSelected];
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"%@ viewDidDisappear", self.nameClass);
    if ([self.fetchedResultsController.fetchedObjects count] > 0) {
        if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
            [self setTimer];
        }
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    NSLog(@"%@ viewDidDisappear", self.nameClass);
    [self stopTimer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@ viewDidLoad", self.nameClass);
    self.navigationController.navigationBar.alpha = 1.0f;
    self.navigationController.navigationBar.translucent = NO;
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:EVENT_NEXT_FETCH_SECOND];
    [self registerNotificationSelectedFilter];
}

- (void) registerNotificationSelectedFilter
{
    [[NSNotificationCenter defaultCenter] addObserverForName:FilterSelected object:nil queue:nil usingBlock:^(NSNotification *note) {
        self.filter = note.userInfo[FilterObject];
        NSLog(@"%@ registerNotificationSelectedFilter %@", self.nameClass, self.filter);
    }];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==0)
    {
        if (section == 0)
        {
            return EVENT_NEXT_FETCH_SECOND;
        }
    }
    return @"";
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.tableView reloadData];
}

- (UIActivityIndicatorView *) activityIndicatorView
{
    if (!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    }
    return _activityIndicatorView;
}

- (void) setViewLabeNetworkError
{
    [self.invalidInputData setHidden:YES];
    self.invalidInputData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 25)];
    self.invalidInputData.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.invalidInputData];
}

- (void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    self.user = [User fetchUser:_managedObjectContext withUserName:[Support getUsername]];

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    request.predicate = [NSPredicate predicateWithFormat:@"whichFilter.whoOwnFilter == %@ ", self.user];
    
    NSSortDescriptor * createdDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
    NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"message" ascending:YES];
    
    [request setSortDescriptors:[NSArray arrayWithObjects:createdDescriptor, nameDescriptor, nil]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
    NSLog(@"%@ fetchedResultsController %d",self.nameClass, [self.fetchedResultsController.fetchedObjects count]);
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fetchedResultsController.fetchedObjects count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (([indexPath row] == [self.fetchedResultsController.fetchedObjects count]) && ([self.fetchedResultsController.fetchedObjects count])) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Next Event"];
        cell.textLabel.text = [NSString stringWithFormat:NEXT_EVENTS,[[Support getLimitFetch] integerValue]];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        self.activityIndicatorView.frame = CGRectMake(SCREEN_WIDTH/2 - 90.0f, 12.0f, 25.0f, 25.0f);
        [self.activityIndicatorView sizeToFit];
        [self.activityIndicatorView setColor:[UIColor grayColor]];
        [cell.contentView addSubview:self.activityIndicatorView];
    } else if ([self.fetchedResultsController.fetchedObjects count]) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell = [[IntegooEventTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"New Event Cell"];
        Event *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
        cell.accessoryType=  UITableViewCellAccessoryDisclosureIndicator;
        if (event.whichIcon.img == nil) {
            cell.imageView.image = [UIImage imageNamed:@"Apple"];
        } else {
            cell.imageView.image = [UIImage imageWithData:event.whichIcon.img];
        }
        cell.textLabel.text = event.message;
        cell.detailTextLabel.text = event.place;
        
        UILabel *labelCell = [[UILabel alloc]init];
        [labelCell setTag:11101];
        labelCell.frame = CGRectMake(SCREEN_WIDTH - 130.0f,10.0f,100.0f,25.0f) ;
        [labelCell setBackgroundColor:[UIColor clearColor]];
        [labelCell setFont:[UIFont systemFontOfSize: 12]];
        [labelCell setTextColor:[UIColor blackColor]];
        
        NSDate *dateCreated = event.created;
        NSTimeInterval seconds = ([[NSDate date] timeIntervalSince1970] - [dateCreated timeIntervalSince1970]);
        labelCell.text = [Support durationTime:seconds];
        labelCell.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:labelCell];
        if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
            [self setTimer];
        }
    } else {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"New Event Cell"];
    }
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == [self.fetchedResultsController.fetchedObjects count]) {
        [self.activityIndicatorView startAnimating];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        if (self.managedObjectContext) {
            NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
            if (base64Encoded != nil) {
                Event *event = [self.fetchedResultsController.fetchedObjects lastObject];
                NSLog(@"%@ last object from %@ and created %f",self.nameClass, event.message, [event.created timeIntervalSince1970]);
                [self checkNetworkConnection:base64Encoded lastCreated:[event.created timeIntervalSince1970] * 1000];
            }
        }
    } else {
        self.selectedEvent = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSLog(@"%@ Event %@", self.nameClass, self.selectedEvent.message);
        [self performSegueWithIdentifier:@"Show New Event" sender:self];
    }
}

- (void) checkNetworkConnection:(NSString *)base64Encoded lastCreated: (NSTimeInterval) created
{
    dispatch_queue_t queue = dispatch_queue_create("network", NULL);
    dispatch_async(queue, ^{
        Reachability *rHostName = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus networkStatus = [rHostName currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_NO_INTERNET_CONNECTION withTimeCreated:created];
            });
        } else {
            NSMutableArray *arrayAllEvents = [[NSMutableArray alloc] init];
            [self.managedObjectContext performBlock:^{
                [arrayAllEvents addObjectsFromArray:[Event fetchAllEvents:self.managedObjectContext withFilter:self.filter]];
                [self.managedObjectContext save:NULL];
            }];
            [self startSesstionDownloadNewEvents:base64Encoded lastCreated:created withOffset:0
                                   withOldEvents:arrayAllEvents
                                     wihNewArray:[[NSMutableArray alloc] init]];
        }
    });
}

- (IBAction)refreshControl:(UIRefreshControl *)sender {
    if (self.filter == nil) {
        [self turnOffNetworkIndicatorAndResfreshControl:FILTER_NO_TITLE withTimeCreated:0];
    } else {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        if (self.managedObjectContext) {
            NSString *base64Encoded = [Support getUsernameAndPasswordForBase64Encoded:nil withPassword:nil];
            if (base64Encoded != nil) {
                [self checkNetworkConnection:base64Encoded lastCreated:0];
            }
        }
    }
}

- (void) startSesstionDownloadNewEvents:(NSString *) base64Encoded lastCreated: (NSTimeInterval)created
                             withOffset:(NSUInteger)offset withOldEvents: (NSArray*)oldEvents wihNewArray: (NSMutableArray *) newArray
{
    NSLog(@"%@ startSesstionDownloadNewEvents", self.nameClass);
    void(^doSomething)(void) = ^(void) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[Support getWholeAddress:URL_EVENTS] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1];
        [request addValue:base64Encoded forHTTPHeaderField:HEADER_AUTHORIZATION];
        [request addValue:HEADER_ACCEPT_CONTENT_TYPE forHTTPHeaderField:HEADER_ACCEPT];
        [request addValue:[NSString stringWithFormat: @"%@", self.filter] forHTTPHeaderField:HEADER_X_FILTER];
        [request addValue:[NSString stringWithFormat: @"%f", created] forHTTPHeaderField:HEADER_X_CREATED];
        [request addValue:[NSString stringWithFormat: @"%@", [Support getLimitFetch]] forHTTPHeaderField:HEADER_X_LIMIT];
        [request addValue:[NSString stringWithFormat: @"%d", (int)offset] forHTTPHeaderField:HEADER_X_OFFSET];
        [request setHTTPMethod:HTTP_METHOD_GET];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        configuration.timeoutIntervalForRequest = TIME_INTERVAL_FOREGRAUND_REQUEST;
        configuration.allowsCellularAccess = NO;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
            completionHandler:^(NSURL *url, NSURLResponse *response, NSError *error) {
                if (!error) {
                    if ([request.URL isEqual:[Support getWholeAddress:URL_EVENTS]]) {
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        if ([httpResponse statusCode] == 200) {
                            NSLog(@"%@ OK - Authentication request url:%@", self.nameClass, [request.URL absoluteString]);
                            NSLog(@"%@ OK - Authentication request header:%@", self.nameClass, [request allHTTPHeaderFields]);
                            NSLog(@"%@ OK - Authentication request body:%@", self.nameClass, [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
                            if (created == 0) {     //created == 0 fetch all database
                                NSArray *newPartArray = [self putIntoCoreData:url intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                                [newArray addObjectsFromArray: newPartArray];
                                NSLog(@"%@ whole events count %lu",self.nameClass, (unsigned long)newArray.count);
                                if (newPartArray.count == [[Support getLimitFetch] integerValue]) {
                                    NSLog(@"%@ offset:%d", self.nameClass, [[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] +
                                          [[Support getLimitFetch] integerValue]);
                                    [self.refreshControl endRefreshing];
                                    [self startSesstionDownloadNewEvents:base64Encoded lastCreated:created withOffset:[[[request allHTTPHeaderFields] valueForKey:HEADER_X_OFFSET] integerValue] + [[Support getLimitFetch] integerValue] withOldEvents:oldEvents wihNewArray:newArray];
                                } else {
                                    [self stopTimer];
                                    [self.managedObjectContext performBlock:^{
                                        [Event deleteOldObjectsFromNSArray:[oldEvents mutableCopy]  compareNewArray:newArray inManagedObjectContext:self.managedObjectContext withFilter:self.filter];
                                        [self.managedObjectContext save:NULL];
                                    }];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if ((self.elapsedTimeTimer == nil) || (![self.elapsedTimeTimer isValid])) {
                                            [self setTimer];
                                        }
                                        [self turnOffNetworkIndicatorAndResfreshControl:nil withTimeCreated:created];
                                    });
                                }
                            } else {    //creted > 0 fetch next limit
                                [self putIntoCoreData:url intoContext:self.managedObjectContext andThenExecuteBlock:nil];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self turnOffNetworkIndicatorAndResfreshControl:nil withTimeCreated:created];
                                });
                            }
                        } else {
                            NSLog(@"%@ ERROR - Authentication response status code: %ld", self.nameClass, (long)[httpResponse statusCode]);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withTimeCreated:created];
                            });
                        }
                    } else {
                        NSLog(@"%@ ERROR - bad request", self.nameClass);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withTimeCreated:created];
                        });
                    }
                } else {
                    NSLog(@"%@ ERROR - bad request", self.nameClass);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self turnOffNetworkIndicatorAndResfreshControl:ALERT_MESSAGE_BAD_NETWORK_REQUEST withTimeCreated:created];
                    });
                }
            }];
        [downloadTask resume];
    };
    doSomething();
}

- (void) turnOffNetworkIndicatorAndResfreshControl:(NSString *)errorMessage withTimeCreated: (NSTimeInterval) created
{
    if(created > 0) {
        [self.activityIndicatorView stopAnimating];
    } else {
        [self.refreshControl endRefreshing];
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if(errorMessage != nil) {
        [self setViewLabeNetworkError];
        [Support wrongAuthentication:errorMessage withLable:self.invalidInputData];
    }
}

- (NSMutableArray *)integooEventsAtURL:(NSURL *)data
{
    NSMutableArray *event;
    NSData *integooJSONData = [NSData dataWithContentsOfURL:data];  // will block if url is not local!
    if (integooJSONData) {
        NSError *error;
        event = [NSJSONSerialization JSONObjectWithData:integooJSONData
                                                options:0
                                                  error:&error];
    }
    return event;
}

- (NSArray *) putIntoCoreData:(NSURL *)url intoContext:(NSManagedObjectContext *)context andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *event = [[self integooEventsAtURL:url] mutableCopy];
        [context performBlock:^{
            [Event loadEventsFromIntegooArray:event intoManagedObjectContext:context withFilter:self.filter withUser:self.user];
            [context save:NULL];
            if (whenDone) whenDone();
        }];
        return event;
    } else {
        if (whenDone) whenDone();
    }
    return nil;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show New Event"]) {
        if ([segue.destinationViewController isKindOfClass:[DetailEventViewController class]]) {
            NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext,EventObject : self.selectedEvent} : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:DetailEventNotification object:self userInfo:userInfo];
        }
    }
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        NSDictionary *userInfo = self.managedObjectContext ? @{DatabaseAvailabityContext : self.managedObjectContext} : nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:ContainerFilterNotification object:self userInfo:userInfo];
    }
}

- (void) setTimer
{
    NSLog(@"%@ setTimer", self.nameClass);
    self.elapsedTimeTimer = [NSTimer scheduledTimerWithTimeInterval:1.000 target:self selector:@selector(reloadTableView) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.elapsedTimeTimer forMode:NSRunLoopCommonModes];
    [self.elapsedTimeTimer fire];
}

- (void) stopTimer
{
    NSLog(@"%@ stopTimer", self.nameClass);
    [self.elapsedTimeTimer invalidate];
    self.elapsedTimeTimer = nil;
}

- (void) reloadTableView
{
    [self.tableView reloadData];
}

@end
